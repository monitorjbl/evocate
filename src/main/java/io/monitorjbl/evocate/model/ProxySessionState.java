package io.monitorjbl.evocate.model;

public enum ProxySessionState {
    START(0),
    SESSION_INITIATING(1),
    AWAITING_CONNECTION(2),
    HANDSHAKE_COMPLETE(3),
    CONNECTED(4),
    SENDING(5),
    RECEIVING(6),
    TUNNELING(7),
    DISCONNECTING(8);

    private final int order;

    ProxySessionState(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }
}
