package io.monitorjbl.evocate.utils;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.core.LayoutBase;
import io.monitorjbl.evocate.session.ProxySession;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;

public class ChannelLogPattern extends LayoutBase<ILoggingEvent> {
    @Override
    public String doLayout(ILoggingEvent event) {
        var sbuf = new StringBuffer(128);
        sbuf.append(ZonedDateTime.ofInstant(Instant.ofEpochSecond(event.getTimeStamp() / 1000), ZoneOffset.UTC).toString());
        sbuf.append(" [");
        sbuf.append(event.getLevel());
        sbuf.append("] [");
        sbuf.append(shortenClassName(event.getLoggerName()));

        if (LoggerFactory.getLogger(event.getLoggerName()).isTraceEnabled()) {
            var stackFrames = event.getCallerData();
            sbuf.append(".");
            sbuf.append(stackFrames[0].getMethodName());
            sbuf.append("(");
            sbuf.append(stackFrames[0].getFileName());
            sbuf.append(":");
            sbuf.append(stackFrames[0].getLineNumber());
            sbuf.append(")");
        }

        if (event.getArgumentArray() != null && event.getArgumentArray().length > 0 && event.getArgumentArray()[0] instanceof ProxySession) {
            var session = (ProxySession) event.getArgumentArray()[0];
            var proxy = session.getProxy();
            sbuf.append("] [");
            sbuf.append(proxy.getPort());
            sbuf.append("::");
            sbuf.append(session.getId());
            sbuf.append("::");
            sbuf.append(session.getCurrentState());
            sbuf.append("]: ");
            sbuf.append(MessageFormatter.arrayFormat(
                    event.getMessage(),
                    Arrays.copyOfRange(event.getArgumentArray(), 1, event.getArgumentArray().length)).getMessage());
        } else {
            sbuf.append("]: ");
            sbuf.append(event.getFormattedMessage());
        }

        if (event.getThrowableProxy() != null) {
            sbuf.append("\n");
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ((ThrowableProxy) event.getThrowableProxy()).getThrowable().printStackTrace(pw);
            sbuf.append(sw.toString());
        }

        sbuf.append("\n");
        return sbuf.toString();
    }

    private static String shortenClassName(String className) {
        var split = className.split("\\.");
        var sb = new StringBuilder();

        for (int i = 0; i < split.length; i++) {
            if (i < split.length - 1) {
                sb.append(Character.toLowerCase(split[i].charAt(0))).append('.');
            } else {
                sb.append(split[i]);
            }
        }

        return sb.toString();
    }
}
