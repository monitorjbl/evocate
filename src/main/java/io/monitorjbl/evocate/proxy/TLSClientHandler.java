package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.session.ProxySession;
import io.monitorjbl.evocate.EvocateConfig;
import io.monitorjbl.evocate.utils.UncheckedInterfaces.UncheckedConsumer;
import io.monitorjbl.evocate.utils.Utils;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.ssl.SslHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLEngine;
import java.util.Optional;

public class TLSClientHandler extends SslHandler {
    private static final Logger LOG = LoggerFactory.getLogger(TLSClientHandler.class);

    private final EvocateConfig config;
    private final ProxySession proxySession;

    public TLSClientHandler(EvocateConfig config, SSLEngine engine, ProxySession proxySession) {
        super(engine);
        this.config = config;
        this.proxySession = proxySession;
        setHandshakeTimeoutMillis(config.getClientTLSHandshakeTimeout());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ifTLS(
                ctx,
                session -> {
                    LOG.trace("TLS read {} {}", proxySession, msg, Utils.logByteBufContent(LOG, msg));
                    super.channelRead(ctx, msg);
                },
                o -> {
                    LOG.trace("Non-TLS read {}", proxySession, Utils.logByteBufContent(LOG, msg));
                    ctx.fireChannelRead(msg);
                });
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        ifTLS(
                ctx,
                s -> {
                    LOG.trace("TLS write {}", proxySession, Utils.logByteBufContent(LOG, msg));
                    super.write(ctx, msg, promise);
                },
                s -> {
                    LOG.trace("Non-TLS write {}", proxySession, Utils.logByteBufContent(LOG, msg));
                    Utils.logByteBufContent(LOG, msg);
                    ctx.writeAndFlush(msg, promise).addListener(f -> {
                        if (!f.isSuccess()) {
                            LOG.warn("Non-TLS write failed", proxySession, f.cause());
                        } else {
                            LOG.debug("Non-TLS write succeeded", proxySession);
                        }
                    });
                }
        );
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.fireChannelActive();
    }

    @Override
    public void flush(ChannelHandlerContext ctx) throws Exception {
        if (proxySession.isTLS()) {
            super.flush(ctx);
        } else {
            ctx.flush();
        }
    }

    @Override
    public void close(ChannelHandlerContext ctx, ChannelPromise promise) throws Exception {
        ctx.close(promise);
    }

    private void ifTLS(ChannelHandlerContext ctx, UncheckedConsumer<ProxySession> ifTLS, UncheckedConsumer<Optional<ProxySession>> noTLS) throws Exception {
        if (proxySession.isTLS()) {
            ifTLS.accept(proxySession);
            return;
        } else {
            noTLS.accept(Optional.of(proxySession));
        }
    }

}
