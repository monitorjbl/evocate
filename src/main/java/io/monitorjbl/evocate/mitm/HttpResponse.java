package io.monitorjbl.evocate.mitm;

public interface HttpResponse {
    HttpProtocol protocol();

    int statusCode();

    String statusText();

    HttpHeaders headers();
}
