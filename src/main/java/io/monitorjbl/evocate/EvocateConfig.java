package io.monitorjbl.evocate;

import io.monitorjbl.evocate.EvocateConfig.VersionProvider;
import io.monitorjbl.evocate.utils.Utils;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IVersionProvider;
import picocli.CommandLine.Option;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Command(versionProvider = VersionProvider.class)
public class EvocateConfig {
    @Option(names = {"-v", "--version"}, versionHelp = true, description = "Display version info")
    private Boolean versionInfoRequested;
    @Option(names = {"-h", "--help"}, usageHelp = true, description = "Display this help message")
    private Boolean usageHelpRequested;
    @Option(names = {"--trace"}, description = "Enables trace logging output")
    private Boolean traceLogging = false;
    @Option(names = {"--debug"}, description = "Enables debug logging output")
    private Boolean debugLogging = false;
    @Option(names = {"--port"}, paramLabel = "PORT", description = "The port to start the REST API on", defaultValue = "9090")
    private Integer apiPort;
    @Option(names = {"--proxy-port-range"}, paramLabel = "PORT_RANGE", description = "The range of ports to assign new proxies", defaultValue = "39500-39510")
    private String proxyPortRange;
    @Option(names = {"--proxy-threads"}, paramLabel = "MILLIS", description = "The number of threads to handle requests for each proxy", defaultValue = "16")
    private Integer proxyThreads;
    @Option(names = {"--sign-tls-certs"}, description = "Whether to sign all certs for MITM operations. This has significant performance implications")
    private Boolean signTLSCerts = false;
    @Option(names = {"--client-connection-timeout"}, paramLabel = "MILLIS", description = "The duration in milliseconds to wait for client connections", defaultValue = "30000")
    private Integer clientConnectionTimeout;
    @Option(names = {"--client-tls-handshake-timeout"}, paramLabel = "MILLIS", description = "The duration in milliseconds to wait for client TLS handhakes", defaultValue = "90000")
    private Integer clientTLSHandshakeTimeout;
    @Option(names = {"--remote-keep-alive"}, description = "Whether to force TCP keep-alive on all remote connections")
    private Boolean remoteConnectionKeepAlive = false;
    @Option(names = {"--remote-connection-timeout"}, paramLabel = "MILLIS", description = "The duration in milliseconds to wait for remote connections", defaultValue = "30000")
    private Integer remoteConnectionTimeout;
    @Option(names = {"--remote-tls-handshake-timeout"}, paramLabel = "MILLIS", description = "The duration in milliseconds to wait for remote TLS handhakes", defaultValue = "90000")
    private Integer remoteTLSHandshakeTimeout;

    public EvocateConfig() { }

    private EvocateConfig(Builder builder) {
        traceLogging = builder.traceLogging;
        debugLogging = builder.debugLogging;
        apiPort = builder.apiPort;
        proxyPortRange = builder.proxyPortRange;
        proxyThreads = builder.proxyThreads;
        signTLSCerts = builder.signTLSCerts;
        clientConnectionTimeout = builder.clientConnectionTimeout;
        clientTLSHandshakeTimeout = builder.clientTLSHandshakeTimeout;
        remoteConnectionKeepAlive = builder.remoteConnectionKeepAlive;
        remoteConnectionTimeout = builder.remoteConnectionTimeout;
        remoteTLSHandshakeTimeout = builder.remoteTLSHandshakeTimeout;
    }

    public Boolean getTraceLogging() {
        return traceLogging;
    }

    public Boolean getDebugLogging() {
        return debugLogging;
    }

    public Integer getApiPort() {
        return apiPort;
    }

    public String getProxyPortRange() {
        return proxyPortRange;
    }

    public Integer getProxyThreads() {
        return proxyThreads;
    }

    public Boolean getSignTLSCerts() {
        return signTLSCerts;
    }

    public Integer getClientConnectionTimeout() {
        return clientConnectionTimeout;
    }

    public Integer getClientTLSHandshakeTimeout() {
        return clientTLSHandshakeTimeout;
    }

    public Boolean getRemoteConnectionKeepAlive() {
        return remoteConnectionKeepAlive;
    }

    public Integer getRemoteConnectionTimeout() {
        return remoteConnectionTimeout;
    }

    public Integer getRemoteTLSHandshakeTimeout() {
        return remoteTLSHandshakeTimeout;
    }

    public List<String> toArgs() {
        return Stream.of(EvocateConfig.class.getDeclaredFields()).filter(f -> f.getAnnotation(Option.class) != null).flatMap(f -> {
            try {
                var opt = f.getAnnotation(Option.class);
                var name = opt.names()[0];
                var val = f.get(this);
                if (f.getType().equals(Boolean.class)) {
                    if (val != null && (Boolean) val) {
                        return Stream.of(name);
                    }
                } else if (val != null) {
                    return Stream.of(name, String.valueOf(val));
                }
                return Stream.of();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }

    public static EvocateConfig parseArgs(Consumer<CommandLine> cliFunc, String... args) {
        var config = new EvocateConfig();
        var cli = new CommandLine(config);
        cli.parseArgs(args);
        cliFunc.accept(cli);
        return config;
    }

    // Creates a new builder with default values pre-populated
    public static Builder builder() {
        var copy = new EvocateConfig();
        new CommandLine(copy).parseArgs();
        Builder builder = new Builder();
        builder.traceLogging = copy.traceLogging;
        builder.debugLogging = copy.debugLogging;
        builder.apiPort = copy.getApiPort();
        builder.proxyPortRange = copy.getProxyPortRange();
        builder.proxyThreads = copy.getProxyThreads();
        builder.signTLSCerts = copy.getSignTLSCerts();
        builder.clientConnectionTimeout = copy.getClientConnectionTimeout();
        builder.clientTLSHandshakeTimeout = copy.getClientTLSHandshakeTimeout();
        builder.remoteConnectionKeepAlive = copy.getRemoteConnectionKeepAlive();
        builder.remoteConnectionTimeout = copy.getRemoteConnectionTimeout();
        builder.remoteTLSHandshakeTimeout = copy.getRemoteTLSHandshakeTimeout();
        return builder;
    }

    public static final class Builder {
        private Boolean traceLogging;
        private Boolean debugLogging;
        private Integer apiPort;
        private String proxyPortRange;
        private Integer proxyThreads;
        private Boolean signTLSCerts;
        private Integer clientConnectionTimeout;
        private Integer clientTLSHandshakeTimeout;
        private Boolean remoteConnectionKeepAlive;
        private Integer remoteConnectionTimeout;
        private Integer remoteTLSHandshakeTimeout;

        private Builder() { }

        public Builder withTraceLogging(Boolean val) {
            this.traceLogging = val;
            return this;
        }

        public Builder withDebugLogging(Boolean val) {
            this.debugLogging = val;
            return this;
        }

        public Builder withApiPort(Integer val) {
            apiPort = val;
            return this;
        }

        public Builder withProxyPortRange(String val) {
            proxyPortRange = val;
            return this;
        }

        public Builder withProxyThreads(Integer val) {
            proxyThreads = val;
            return this;
        }

        public Builder withSignTLSCerts(Boolean val) {
            signTLSCerts = val;
            return this;
        }

        public Builder withClientConnectionTimeout(Integer val) {
            clientConnectionTimeout = val;
            return this;
        }

        public Builder withClientTLSHandshakeTimeout(Integer val) {
            clientTLSHandshakeTimeout = val;
            return this;
        }

        public Builder withRemoteConnectionKeepAlive(Boolean val) {
            remoteConnectionKeepAlive = val;
            return this;
        }

        public Builder withRemoteConnectionTimeout(Integer val) {
            remoteConnectionTimeout = val;
            return this;
        }

        public Builder withRemoteTLSHandshakeTimeout(Integer val) {
            remoteTLSHandshakeTimeout = val;
            return this;
        }

        public EvocateConfig build() {
            return new EvocateConfig(this);
        }
    }

    public static class VersionProvider implements IVersionProvider {

        @Override
        public String[] getVersion() throws Exception {
            var is = VersionProvider.class.getClassLoader().getResourceAsStream("META-INF/evocate/info.json");
            if (is == null) {
                return new String[]{"Version info not found"};
            } else {
                var info = Utils.readAsJSON(is);
                return new String[]{
                        String.format("evocate version \"%s\" (built from commit %s on %s)",
                                info.get("version"),
                                info.get("commit"),
                                info.get("buildDate"))
                };
            }
        }
    }
}
