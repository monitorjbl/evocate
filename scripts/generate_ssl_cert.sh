#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CA_NAME=evocate
CERT_NAME=evocate
PASSWORD=evocate

pushd ${DIR}/../src/main/resources/certs
rm -f ./*
set -x

# Generate a root CA private key
openssl genrsa -des3 -out evocateCA.key -passout pass:${PASSWORD} 2048

# Generate a root CA public cert
openssl req -x509 -new -nodes -key evocateCA.key -sha256 -days 1825 -out evocateCA.crt -passin pass:${PASSWORD} -subj "/C=US/ST=NC/L=RTP/O=Evocate/CN=Evocate Root CA"

# Generate self-signed cert for our site
openssl genrsa -out evocate.key 2048

# Create a root-signed cert using the self-signed cert
openssl req -new -key evocate.key -out evocate.csr -subj "/C=US/ST=NC/L=RTP/O=Evocate/CN=evocate"

# Generate the config needed to merge everything together
echo 'authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = evocate
' > ./evocate.ext

# Merge it all together into one monstrosity-cert
openssl x509 -req -in evocate.csr -CA evocateCA.crt -CAkey evocateCA.key -CAcreateserial -out evocate.crt -days 1825 -sha256 -extfile evocate.ext -passin pass:${PASSWORD}

# Generate a keystore we can use to serve this key at runtime
openssl pkcs12 -export -in evocate.crt -inkey evocate.key -out evocate.p12 -name "evocate" -passin pass:${PASSWORD} -passout pass:${PASSWORD}
keytool -importkeystore -destkeystore evocate.jks -deststorepass ${PASSWORD} -srckeystore evocate.p12 -srcstoretype PKCS12 -srcstorepass ${PASSWORD}

popd
