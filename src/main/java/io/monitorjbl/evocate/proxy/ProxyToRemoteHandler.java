package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.mitm.EventSource;
import io.monitorjbl.evocate.mitm.MITM;
import io.monitorjbl.evocate.session.ProxySession;
import io.monitorjbl.evocate.session.ProxySessionStateMachine;
import io.monitorjbl.evocate.session.ProxySessionStateMachine.Direction;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledFuture;

public class ProxyToRemoteHandler extends ChannelDuplexHandler {
    private static final Logger LOG = LoggerFactory.getLogger(ProxyToRemoteHandler.class);

    private final ProxySessionStateMachine stateMachine;
    private final ProxySession proxySession;
    private final MITM mitm;
    private final Queue<Object> backupQueue = new ConcurrentLinkedQueue<>();

    private ScheduledFuture backupFuture;

    public ProxyToRemoteHandler(ProxySessionStateMachine stateMachine, ProxySession proxySession, MITM mitm) {
        this.stateMachine = stateMachine;
        this.proxySession = proxySession;
        this.mitm = mitm;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        LOG.debug("server read: {}", proxySession, msg.getClass().getName());
        stateMachine.sessionStateMachine(ctx, Direction.IN, msg);
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        LOG.debug("Writing {} to server", proxySession, msg.getClass().getName());
        super.write(ctx, msg, promise);
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        LOG.debug("Server channel unregistered", proxySession);
        stateMachine.disconnect("Server channel unregistered", EventSource.SERVER_SIDE);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        // This is caused by a server closing the connection without warning. It's not an
        // error on our side, so log it only at the debug level
        if (cause instanceof IOException && cause.getMessage().equals("Connection reset by peer")) {
            LOG.debug("Server connection lost", proxySession, cause);
        } else {
            LOG.error("ERR", cause);
        }
        stateMachine.channelError(EventSource.SERVER_SIDE, cause);
    }

}
