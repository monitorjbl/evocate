package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.mitm.MITM;
import io.monitorjbl.evocate.session.ProxySession;
import io.monitorjbl.evocate.session.ProxySessionStateMachine;
import io.monitorjbl.evocate.tls.TLSHandlerProvider;
import io.monitorjbl.evocate.EvocateConfig;
import io.monitorjbl.evocate.utils.Utils;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.ByteBufFormat;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.Optional;

public class RemoteChannel {
    private static final Logger LOG = LoggerFactory.getLogger(RemoteChannel.class);

    private final EvocateConfig config;
    private final EventLoopGroup workerGroup;
    private final String hostname;
    private final int port;
    private final ProxySessionStateMachine stateMachine;
    private final ProxySession proxySession;
    private final MITM mitm;

    private Channel serverChannel;

    public RemoteChannel(EvocateConfig config, EventLoopGroup workerGroup, ProxySessionStateMachine stateMachine, ProxySession proxySession,
                         String hostname, int port, MITM mitm) {
        this.config = config;
        this.workerGroup = workerGroup;
        this.stateMachine = stateMachine;
        this.proxySession = proxySession;
        this.hostname = hostname;
        this.port = port;
        this.mitm = mitm;
    }

    public ChannelFuture openConnection() {
        LOG.debug("Opening connection to {}:{}", proxySession, hostname, port);

        var clientBootstrap = new Bootstrap()
                .group(workerGroup)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.SO_KEEPALIVE, config.getRemoteConnectionKeepAlive())
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, config.getRemoteConnectionTimeout())
                .remoteAddress(new InetSocketAddress(hostname, port))
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        var httpResponseDecoder = new MaybeHttpResponseDecoder(proxySession);
                        var httpRequestEncoder = new MaybeHttpRequestEncoder(proxySession);
                        var websocketDecoder = new MaybeWebsocketDecoder(proxySession);
                        var websocketEncoder = new MaybeWebsocketEncoder(proxySession, true);
                        var proxyToServerHandler = new ProxyToRemoteHandler(stateMachine, proxySession, mitm);

                        ch.pipeline().addLast("TLSClientHandler", TLSHandlerProvider.getClientTLSHandler(config, proxySession));

                        if (LOG.isTraceEnabled()) {
                            ch.pipeline().addLast("LOGGER", new LoggingHandler(RemoteChannel.class, LogLevel.TRACE, ByteBufFormat.HEX_DUMP));
                        }

                        ch.pipeline().addLast("WebsocketDecoder", websocketDecoder);
                        ch.pipeline().addLast("WebsocketEncoder", websocketEncoder);
                        ch.pipeline().addLast("HttpResponseDecoder", httpResponseDecoder);
                        ch.pipeline().addLast("HttpRequestEncoder", httpRequestEncoder);
                        ch.pipeline().addLast("ProxyToRemoteHandler", proxyToServerHandler);

                        if (LOG.isTraceEnabled()) {
                            LOG.trace("Server-side pipeline: {}", proxySession, Utils.getPipelineDescription(ch.pipeline()));
                        }
                    }
                });
        var channelFuture = clientBootstrap.connect();
        channelFuture.addListener(future -> {
            if (future.isSuccess()) {
                LOG.debug("Successfully connected to {}:{}", proxySession, hostname, port);
                this.serverChannel = channelFuture.channel();
            } else {
                LOG.warn("Failed to connect to {}:{}", proxySession, hostname, port, future.cause());
                channelFuture.channel().close();
            }
        });

        return channelFuture;
    }

    public ChannelFuture writeAndFlush(Object msg) {
        if (serverChannel == null) {
            LOG.warn("Write before server is ready!", proxySession);
            return new ImmediateChannelFuture(serverChannel, new IllegalStateException("Server not yet ready"));
        }
        if (serverChannel.isWritable()) {
            LOG.debug("Writing {}", proxySession, msg.getClass().getName());
            return serverChannel.writeAndFlush(msg);
        } else {
            LOG.debug("Channel is not writable, not writing {}", proxySession, msg.getClass().getName());
            return new ImmediateChannelFuture(serverChannel, new IllegalStateException("Channel not writable"));
        }
    }

    public String getHost() {
        return String.format("%s:%s", hostname, port);
    }

    public Optional<ChannelFuture> close() {
        if (serverChannel != null) {
            return Optional.of(this.serverChannel.close());
        } else {
            return Optional.empty();
        }
    }
}
