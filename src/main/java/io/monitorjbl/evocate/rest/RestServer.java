package io.monitorjbl.evocate.rest;

public interface RestServer {
    void run();

    boolean isRunning();

    void close();
}
