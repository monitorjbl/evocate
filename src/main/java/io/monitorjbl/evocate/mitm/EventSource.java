package io.monitorjbl.evocate.mitm;

public enum EventSource {
    CLIENT_SIDE, SERVER_SIDE
}
