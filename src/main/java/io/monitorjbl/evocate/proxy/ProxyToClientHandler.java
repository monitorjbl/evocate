package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.mitm.EventSource;
import io.monitorjbl.evocate.mitm.MITM;
import io.monitorjbl.evocate.session.ProxySession;
import io.monitorjbl.evocate.session.ProxySessionStateMachine;
import io.monitorjbl.evocate.session.ProxySessionStateMachine.Direction;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.DecoderException;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.LastHttpContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;

public class ProxyToClientHandler extends ChannelDuplexHandler {
    private static final Logger LOG = LoggerFactory.getLogger(ProxyToClientHandler.class);
    private static final String PROXY_SUCCESS_RESPONSE = "%s 200 Connection established\r\n\n";
    private static final String PROXY_FAILURE_RESPONSE = "%s 500 Connection failed\r\n\n";

    private final ProxySessionStateMachine stateMachine;
    private final ProxySession proxySession;
    private final MITM mitm;

    public ProxyToClientHandler(ProxySessionStateMachine stateMachine, ProxySession proxySession, MITM mitm) {
        this.stateMachine = stateMachine;
        this.proxySession = proxySession;
        this.mitm = mitm;
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        LOG.debug("Client channel unregistered", proxySession);
        stateMachine.disconnect("Client channel unregistered", EventSource.CLIENT_SIDE);
        super.channelUnregistered(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        LOG.debug("read {}", proxySession, msg.getClass().getName());
        if (msg instanceof HttpRequest) {
            var request = (HttpRequest) msg;
            // Detect when a websocket upgrade is being requested
            if ("websocket".equalsIgnoreCase(request.headers().get("Upgrade"))) {
                LOG.debug("Detected websocket upgrade request", proxySession);
                proxySession.setWebsocketUpgrading(true);
            }
        }
        stateMachine.sessionStateMachine(ctx, Direction.OUT, msg);
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        ctx.writeAndFlush(msg, promise).addListener((ChannelFutureListener) f -> {
            if (msg instanceof HttpResponse) {
                var response = (HttpResponse) msg;
                // Detect when a websocket upgrade happened
                if ("websocket".equalsIgnoreCase(response.headers().get("Upgrade"))) {
                    LOG.debug("Detected websocket upgrade response", proxySession);
                    proxySession.setWebsocketUpgrading(true);
                }
            }

            if (msg instanceof LastHttpContent) {
                if (proxySession.isWebsocketUpgrading()) {
                    LOG.debug("Websocket upgrade complete, switching to websocket mode", proxySession);
                    proxySession.setWebsocketUpgrading(false);
                    proxySession.setWebsocket(true);
                    stateMachine.sessionStateMachine(ctx, Direction.OUT, msg);
                } else if (!proxySession.isWebsocket()) {
                    LOG.debug("End of response detected, closing session", proxySession);
                    try {
                        stateMachine.disconnect("Detected end of response", EventSource.CLIENT_SIDE);
                    } catch (Exception e) {
                        LOG.error("Failed to close proxySession", e);
                    }
                }
            }
        });
    }

    @Override
    @SuppressWarnings("deprecation")
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (cause instanceof IOException && "Connection reset by peer".equals(cause.getMessage())) {
            // This is caused by a client closing the connection without warning. It's not an
            // error on our side, so log it only at the debug level
            LOG.debug("Client connection lost", proxySession, cause);
        } else if (cause instanceof DecoderException && cause.getCause() instanceof SSLHandshakeException && cause.getCause().getMessage().equals("Received fatal alert: bad_certificate")) {
            // This is caused by a client rejecting our certificate. It doesn't mean anything is
            // wrong on our end, just that they didn't like it for some reason.
            LOG.warn("Client rejected our TLS certificate", proxySession);
        } else {
            LOG.error("Unexpected error", proxySession, cause);
        }
        stateMachine.channelError(EventSource.CLIENT_SIDE, cause);
    }

}
