FROM openjdk:11-jdk as build
COPY build/distributions/evocate.tar /tmp/evocate.tar
RUN cd /tmp && tar xvf ./evocate.tar

FROM openjdk:11-jdk
ARG GIT_COMMIT=unspecified
LABEL maintainer="monitorjbl@gmail.com" \
      git_commit=$GIT_COMMIT
COPY --from=build /tmp/evocate /opt/evocate
ENV JAVA_OPTS="-Xms32m -Xmx256m"
ENTRYPOINT ["/opt/evocate/bin/evocate"]