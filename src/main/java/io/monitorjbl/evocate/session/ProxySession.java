package io.monitorjbl.evocate.session;

import io.monitorjbl.evocate.mitm.MITM;
import io.monitorjbl.evocate.model.BandwidthLimit;
import io.monitorjbl.evocate.model.Proxy;
import io.monitorjbl.evocate.model.ProxySessionState;
import io.monitorjbl.evocate.proxy.RemoteChannel;
import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import io.netty.channel.EventLoopGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProxySession implements Comparable {
    private static final Logger LOG = LoggerFactory.getLogger(ProxySession.class);

    private final Proxy proxy;
    private final ChannelId id;
    private final Channel clientSideChannel;
    private final MITM mitm;
    private final BandwidthLimit bandwidthLimit;

    private ProxySessionState currentState;
    private RemoteChannel remoteChannel;
    private String targetHost;
    private int targetPort;
    private boolean isTunneling;
    private boolean isTLS;
    private boolean isWebsocketUpgrading;
    private boolean isWebsocket;

    public ProxySession(ChannelId id, Channel clientSideChannel, MITM mitm, BandwidthLimit bandwidthLimit, EventLoopGroup serverFacingGroup, Proxy proxy) {
        this.id = id;
        this.clientSideChannel = clientSideChannel;
        this.mitm = mitm;
        this.bandwidthLimit = bandwidthLimit;
        this.proxy = proxy;
        this.currentState = ProxySessionState.START;
    }

    public Proxy getProxy() {
        return proxy;
    }

    public ChannelId getId() {
        return id;
    }

    Channel getClientSideChannel() {
        return clientSideChannel;
    }

    void setServerChannel(RemoteChannel remoteChannel) {
        this.remoteChannel = remoteChannel;
    }

    public RemoteChannel getServerChannel() {
        return remoteChannel;
    }

    public ProxySessionState getCurrentState() {
        return currentState;
    }

    public MITM getMitm() {
        return mitm;
    }

    public BandwidthLimit getBandwidthLimit() {
        return bandwidthLimit;
    }

    public String getTargetHost() {
        return targetHost;
    }

    void setTargetHost(String targetHost) {
        this.targetHost = targetHost;
    }

    public int getTargetPort() {
        return targetPort;
    }

    void setTargetPort(int targetPort) {
        this.targetPort = targetPort;
    }

    public boolean isTunneling() {
        return isTunneling;
    }

    public void setTunneling(boolean tunneling) {
        isTunneling = tunneling;
    }

    public boolean isTLS() {
        return isTLS;
    }

    public void setTLS(boolean TLS) {
        isTLS = TLS;
    }

    public boolean isWebsocketUpgrading() {
        return isWebsocketUpgrading;
    }

    public void setWebsocketUpgrading(boolean websocketUpgrading) {
        isWebsocketUpgrading = websocketUpgrading;
    }

    public boolean isWebsocket() {
        return isWebsocket;
    }

    public void setWebsocket(boolean websocket) {
        isWebsocket = websocket;
    }

    public boolean isInState(ProxySessionState state) {
        return currentState.equals(state);
    }

    void setCurrentState(ProxySessionState currentState) {
        this.currentState = currentState;
    }

    void close() throws Exception {
        synchronized (this) {
            if (this.currentState != ProxySessionState.DISCONNECTING) {
                LOG.info("Closing session", this);
                setCurrentState(ProxySessionState.DISCONNECTING);

                this.clientSideChannel.close().addListener(f -> LOG.debug("Client channel closed", this));
                if (this.remoteChannel != null) {
                    this.remoteChannel.close().ifPresent(p -> p.addListener(f -> LOG.debug("Remote channel closed", this)));
                }
            }
        }
    }

    @Override
    public String toString() {
        return "ProxySession{" +
                "id=" + id +
                ", currentState=" + currentState +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof ProxySession) {
            return getId().compareTo(((ProxySession) o).getId());
        }
        return 0;
    }
}
