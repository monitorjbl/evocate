package io.monitorjbl.evocate.mitm.impl;

import io.monitorjbl.evocate.mitm.HttpHeader;
import io.monitorjbl.evocate.mitm.HttpHeaders;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

public class HttpHeadersImpl implements HttpHeaders {
    private static final HttpHeader EMPTY_HEADER = new HttpHeaderImpl("", asList(""));
    private final Map<String, HttpHeader> headers;

    public HttpHeadersImpl(@Nonnull List<HttpHeader> headers) {
        this.headers = headers.stream().collect(toMap(HttpHeader::name, identity()));
    }

    @Override
    public Set<String> names() {
        return headers.keySet();
    }

    @Override
    public String get(String name) {
        if (headers.containsKey(name)) {
            return headers.get(name).get();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return headers.values().stream()
                .map(HttpHeader::toString)
                .collect(joining("\n"));
    }
}
