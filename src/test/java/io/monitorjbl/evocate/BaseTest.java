package io.monitorjbl.evocate;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.monitorjbl.evocate.utils.servers.EvocateServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;

public abstract class BaseTest {
    protected static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    protected static EvocateServer EVOCATE_SERVER;

    @BeforeAll
    public static void setUp() {
        EVOCATE_SERVER = EvocateServer.startServer();
    }

    @AfterAll
    public static void cleanUp() {
        EVOCATE_SERVER.stopServer();
    }

    @AfterEach
    public void tearDown() {
        EVOCATE_SERVER.clearProxies();
    }
}
