package io.monitorjbl.evocate.store;

import io.monitorjbl.evocate.proxy.ProxyChannel;

public class ProxyChannelStore extends AbstractStore<String, ProxyChannel> {}
