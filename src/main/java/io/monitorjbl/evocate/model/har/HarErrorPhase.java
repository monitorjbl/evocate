package io.monitorjbl.evocate.model.har;

public enum HarErrorPhase {
    CONNECTION, TLS_HANDSHAKE,  REQUEST, RESPONSE
}
