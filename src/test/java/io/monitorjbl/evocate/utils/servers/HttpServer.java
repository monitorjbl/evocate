package io.monitorjbl.evocate.utils.servers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.monitorjbl.evocate.tls.BlindTrustManager;
import io.monitorjbl.evocate.utils.TestUtils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.net.JksOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class HttpServer extends AbstractVerticle {
    private static final Logger LOG = LoggerFactory.getLogger(HttpServer.class);
    private static final ObjectMapper object_mapper = new ObjectMapper();
    private static final Map<Integer, HttpClient> http_clients = new ConcurrentHashMap<>();

    private static int http_port = -1;
    private static int https_port = -1;
    private static HttpServer server;

    private HttpServer() {
        this.vertx = Vertx.vertx();
        io.vertx.core.http.HttpServer http = vertx.createHttpServer(new HttpServerOptions()
                .setReceiveBufferSize(16));
        io.vertx.core.http.HttpServer https = vertx.createHttpServer(new HttpServerOptions()
                .setReceiveBufferSize(5 * 1024 * 1024)
                .setSsl(true)
                .setKeyStoreOptions(new JksOptions()
                        .setPath(HttpServer.class.getClassLoader().getResource("certs/evocate.jks").getFile())
                        .setPassword("evocate")));

        http.requestHandler(handleMessages(false)).listen(http_port);
        https.requestHandler(handleMessages(true)).listen(https_port);
    }

    private void jsonEndpoint(HttpServerRequest ctx) {
        ctx.response()
                .setStatusCode(200)
                .putHeader("Content-Type", "application/json")
                .end("{}");
    }

    private void disconnectEndpoint(HttpServerRequest ctx) {
        if (ctx.method() == HttpMethod.POST || ctx.method() == HttpMethod.PUT) {
            ctx.handler(rc -> {
                var body = rc.getBytes(0, 10);
                ctx.connection().close();
            });
        } else {
            ctx.connection().close();
        }

    }

    private Handler<HttpServerRequest> handleMessages(boolean tls) {
        return ctx -> {
            LOG.info("Received message (tls:{}): {} {}", tls, ctx.method(), ctx.uri());
            if (ctx.uri().equals("/disconnect")) {
                disconnectEndpoint(ctx);
            } else {
                jsonEndpoint(ctx);
            }
        };
    }

    public static void startServer() {
        if (server == null) {
            http_port = TestUtils.getOpenPort();
            https_port = TestUtils.getOpenPort();
            LOG.info("Starting test HTTP server on http:{}/https:{}", http_port, https_port);
            server = new HttpServer();
        }
    }

    public static int getHttpPort() {
        return http_port;
    }

    public static int getHttpsPort() {
        return https_port;
    }

    public static void stopServer() {
        if (server != null) {
            LOG.info("Stopping test server on http:{}/https:{}", http_port, https_port);
            try {
                server.vertx.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                http_port = -1;
                https_port = -1;
                server = null;
            }
        }
    }

    public static JsonNode httpRequest(int proxy, String path, Function<HttpRequest.Builder, HttpRequest> requestBuilder) {
        var client = getClient(proxy);
        var request = requestBuilder.apply(HttpRequest.newBuilder()
                .uri(URI.create(String.format("http://localhost:%s/%s", http_port, stripLeadingSlash(path)))));
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            return object_mapper.readValue(response.body(), JsonNode.class);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static JsonNode httpsRequest(int proxy, String path, Function<HttpRequest.Builder, HttpRequest> requestBuilder) {
        var client = getClient(proxy);
        var request = requestBuilder.apply(HttpRequest.newBuilder()
                .uri(URI.create(String.format("https://localhost:%s/%s", https_port, stripLeadingSlash(path)))));
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            return object_mapper.readValue(response.body(), JsonNode.class);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    static SSLContext getSSLSocketFactory() {
        try {
            var trustAllSslContext = SSLContext.getInstance("SSL");
            trustAllSslContext.init(null, new TrustManager[]{new BlindTrustManager()}, new java.security.SecureRandom());
            return trustAllSslContext;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String stripLeadingSlash(String path) {
        if (path != null && path.length() > 0 && path.charAt(0) == '/') {
            return path.substring(1);
        } else {
            return path;
        }
    }

    private static HttpClient getClient(int proxy) {
        synchronized (http_clients) {
            if (!http_clients.containsKey(proxy)) {
                http_clients.put(proxy, TestUtils.httpClient(Optional.of(proxy)));
            }
            return http_clients.get(proxy);
        }
    }
}
