package io.monitorjbl.evocate.utils;

import io.monitorjbl.evocate.BaseTest;
import io.monitorjbl.evocate.tls.BlindTrustManager;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.google.common.collect.Lists.newArrayList;

public class LoadTest extends BaseTest {
    private static final Logger LOG = LoggerFactory.getLogger(LoadTest.class);
    private static final Random RANDOM = new Random();

    public static void main(String[] args) throws Exception {
        AtomicInteger counter = new AtomicInteger(0);
        for (int i = 39500; i < 39504; i++) {
            var port = i;
            new Thread(() -> {
                stressTest(port);
                counter.incrementAndGet();
            }).start();
        }

        while (counter.get() < 4) {
            Thread.sleep(100);
        }
        LOG.info("Test complete");
    }

    private static void stressTest(int proxy) {
        try {
            var client = new OkHttpClient.Builder()
                    .proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("localhost", proxy)))
                    .sslSocketFactory(getSSLSocketFactory().getSocketFactory(), new BlindTrustManager())
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    })
                    .build();
            var counter = new AtomicInteger(0);
            var threadpool = Executors.newFixedThreadPool(16, r -> new Thread(r, "stress-test-" + proxy + counter.getAndIncrement()));
            var futures = IntStream.range(0, 1024)
                    .mapToObj(i -> threadpool.submit(() -> makeRequest(client, URLS.get(RANDOM.nextInt(URLS.size())))))
                    .collect(Collectors.toList());

            LOG.info("Started test against proxy {}", proxy);
            for (var f : futures) {
                f.get();
            }

            threadpool.shutdown();
            threadpool.awaitTermination(5, TimeUnit.MINUTES);
            LOG.info("Completed test against proxy {}", proxy);
        } catch (Exception e) {
            LOG.error("BARF on proxy {}", proxy, e);
        }
    }

    private static void makeRequest(OkHttpClient client, String url) {
        var request = new Request.Builder()
                .get()
                .url(url)
                .build();
        try (var response = client.newCall(request).execute()) {
            var code = response.code();
            var body = response.body().bytes();
            LOG.info("GET {} ({}): {} bytes", url, code, body.length);
        } catch (IOException e) {
            LOG.error("GET {} failed!", url, e);
            throw new UncheckedIOException(e);
        }
    }

    private static SSLContext getSSLSocketFactory() {
        try {
            var trustAllSslContext = SSLContext.getInstance("SSL");
            trustAllSslContext.init(null, new TrustManager[]{new BlindTrustManager()}, new java.security.SecureRandom());
            return trustAllSslContext;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static final List<String> URLS = newArrayList(
            "https://sdk-cdn.mypurecloud.com/web-telemetry/1.1.1/purecloud-web-telemetry.min.js",
            "https://dhqbrvplips7x.cloudfront.net/directory/9.13.0-2/assets/vendor-1dbbfea28893f135554067c821e69afc.js",
            "https://dhqbrvplips7x.cloudfront.net/directory/9.13.0-2/assets/web-directory-a0d361a9176ecb0914875512e80157ad.js",
            "https://dhqbrvplips7x.cloudfront.net/directory/9.13.0-2/assets/vendor-10439456dcfdc76da9e5c71e4d734f4b.css",
            "https://dhqbrvplips7x.cloudfront.net/directory/9.13.0-2/assets/web-directory-6eeaedd1224f62fb805ecede3193ccb3.css",
            "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/core.js",
            "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js",
            "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css",
            "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/fontawesome.css",
            "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/js/all.js",
            "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/webfonts/fa-brands-400.svg",
            "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/webfonts/fa-brands-400.woff2",
            "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css",
            "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.js",
            "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js",
            "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/uk.js",
            "https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js",
            "https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.runtime.js"
    );
}
