package io.monitorjbl.evocate.mitm;

public interface MITM {
    void onConnectStart(String channelId, String host, int port);

    void onConnectComplete(String channelId);

    void onTLSStart(String channelId);

    void onTLSComplete(String channelId);

    void onRequestStart(String channelId, HttpRequest request);

    void onRequestData(String channelId, byte[] data);

    void onRequestComplete(String channelId);

    void onResponseStart(String channelId, HttpResponse response);

    void onResponseData(String channelId, byte[] data);

    void onResponseComplete(String channelId);

    void onDisconnect(String channelId, EventSource eventSource);

    void onError(String channelId, EventSource eventSource, Throwable cause);
}
