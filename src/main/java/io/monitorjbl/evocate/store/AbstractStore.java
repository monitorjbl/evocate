package io.monitorjbl.evocate.store;

import io.netty.channel.ChannelId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toList;

public class AbstractStore<K, V> {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractStore.class);
    private final Map<K, V> map = new ConcurrentHashMap<>();

    public boolean exists(K id) {
        return map.containsKey(id);
    }

    public List<K> listKeys() {
        return map.keySet().stream()
                .sorted()
                .collect(toList());
    }

    public List<V> listValues() {
        return map.values().stream().collect(toList());
    }

    public V get(K id) {
        return map.get(id);
    }

    public V put(K id, V session) {
        return map.put(id, session);
    }

    public V remove(K id) {
        return map.remove(id);
    }
}
