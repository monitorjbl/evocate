package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.session.ProxySession;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.WebSocket13FrameDecoder;
import io.netty.handler.codec.http.websocketx.WebSocketDecoderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MaybeWebsocketDecoder extends WebSocket13FrameDecoder {
    private static final Logger LOG = LoggerFactory.getLogger(MaybeWebsocketDecoder.class);
    private final ProxySession proxySession;

    public MaybeWebsocketDecoder(ProxySession proxySession) {
        super(WebSocketDecoderConfig.newBuilder()
                .allowMaskMismatch(true)
                .build());
        this.proxySession = proxySession;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (proxySession.isWebsocket()) {
            LOG.debug("Decoding websocket request {}", proxySession, msg.getClass().getName());
            super.channelRead(ctx, msg);
        } else {
            LOG.trace("Not decoding websocket request", proxySession);
            ctx.fireChannelRead(msg);
        }
    }
}
