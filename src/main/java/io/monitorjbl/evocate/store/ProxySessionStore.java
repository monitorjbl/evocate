package io.monitorjbl.evocate.store;

import io.monitorjbl.evocate.session.ProxySession;
import io.netty.channel.ChannelId;

import java.util.List;
import java.util.stream.Collectors;

public class ProxySessionStore extends AbstractStore<ChannelId, ProxySession> {

    @Override
    public List<ProxySession> listValues() {
        return super.listValues().stream()
                .sorted(ProxySession::compareTo)
                .collect(Collectors.toList());
    }
}
