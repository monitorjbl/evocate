package io.monitorjbl.evocate.model.har;

public enum HttpMethod {
    GET, POST, PUT, HEAD, PROPFIND, OPTIONS, REPORT, DELETE, CONNECT, TRACE, CCM_POST, PATCH;
}
