package io.monitorjbl.evocate.rest;

import io.monitorjbl.evocate.service.ProxyService;
import io.monitorjbl.evocate.store.ProxyChannelStore;
import io.monitorjbl.evocate.store.ProxyStore;
import io.monitorjbl.evocate.EvocateConfig;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.StaticHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EvocateRestServer extends AbstractVerticle implements RestServer {
    private static final Logger LOG = LoggerFactory.getLogger(EvocateRestServer.class);

    private final EvocateConfig config;
    private final ProxyResources proxyResources;
    private HttpServer server;
    private boolean running;

    public EvocateRestServer(EvocateConfig config) {
        this.config = config;
        this.proxyResources = new ProxyResources(new ProxyService(config, new ProxyStore(), new ProxyChannelStore()));
    }

    private void loggingHandler(RoutingContext routingContext) {
        var start = System.currentTimeMillis();
        var request = routingContext.request();
        routingContext.next();
        var duration = System.currentTimeMillis() - start;
        LOG.debug("{} {} => {} ({}ms)", request.method(), request.uri(), routingContext.response().getStatusCode(), duration);
    }

    public void run() {
        LOG.debug("Starting REST server");
        var vertx = Vertx.vertx();
        var router = Router.router(vertx);

        // Set up routes
        // To disable static resource caching (useful when doing
        // local development), set the following system property:
        //
        //  -Dvertx.disableFileCaching=true
        router.route().handler(this::loggingHandler);
        router.get("/").handler(ctx -> ctx.response()
                .putHeader("location", "/ui/")
                .setStatusCode(302)
                .end());
        router.route("/ui/*").handler(StaticHandler.create("ui"));
        router.post("/proxy").handler(proxyResources::createProxy);
        router.get("/proxy").handler(proxyResources::listProxy);
        router.get("/proxy/:port").handler(proxyResources::getProxy);
        router.delete("/proxy/:port").handler(proxyResources::deleteProxy);
        router.put("/proxy/:port/har").handler(proxyResources::putHar);
        router.get("/proxy/:port/har").handler(proxyResources::getHar);
        router.delete("/proxy/:port/har").handler(proxyResources::deleteHar);
        router.put("/proxy/:port/limits").handler(proxyResources::putLimits);
        router.get("/proxy/:port/sessions").handler(proxyResources::getSessions);

        vertx.deployVerticle(this);
        this.server = vertx.createHttpServer()
                .requestHandler(router)
                .listen(config.getApiPort());

        LOG.info("REST Server started on port {}", config.getApiPort());
        running = true;
    }

    public boolean isRunning() {
        return running;
    }

    public void close() {
        this.running = false;
        this.proxyResources.close();
        this.server.close();
    }
}
