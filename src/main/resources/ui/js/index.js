async function listProxies() {
    return (await axios.get('/proxy')).data;
}

async function createProxy() {
    return (await axios.post('/proxy')).data;
}

async function deleteProxy(port) {
    await axios.delete(`/proxy/${port}`);
}

Vue.component('app-frame', {
    template: `
<div class="frame columns">
    <div class="column col-2 left-panel">
        <ul class="menu">
            <li>
                <img src="img/evocate.png" class="img-responsive" alt="...">
            </li>
            <li class="divider" data-content="EVOCATE"></li>
          
            <li class="menu-item" v-for="proxy in proxies">
                <a href="#" @click="selectProxy(proxy)">
                    <i class="icon icon-link"></i> Port {{proxy.port}}
                </a>
                <div class="menu-badge">
                    <button class="btn btn-error btn-sm" @click="removeProxy(proxy)">x</button>
                </div>
            </li>
            
            <div class="empty proxy-not-present" v-if="proxies.length == 0">
                <div class="empty-icon">
                    <i class="icon icon-people"></i>
                </div>
                <p class="empty-title h5">No proxies active</p>
                <div class="empty-action">
                    <button class="btn btn-primary" @click="newProxy()">Create a proxy</button>
                </div>
            </div>
            <div class="proxy-present" v-else>
                <button class="btn btn-primary" @click="newProxy()">+ Proxy</button>
            </div>
        </ul>
    </div>
    <div class="column col-10 right-panel">
        <proxy-details v-bind:proxy="selectedProxy" v-if="selectedProxy"/>
    </div>
</div>
    `,
    data: function () {
        return {
            proxies: [],
            selectedProxy: null,
        };
    },
    async created() {
        await this.load();
    },
    methods: {
        async load() {
            this.proxies = await listProxies();
            if (this.proxies.length === 0) {
                this.selectedProxy = null;
            } else if (this.proxies.length === 1) {
                await this.selectProxy(this.proxies[0]);
            }
        },
        async selectProxy(proxy) {
            this.selectedProxy = proxy;
        },
        async newProxy() {
            await createProxy();
            await this.load();
        },
        async removeProxy(proxy) {
            await deleteProxy(proxy.port);
            await this.load();
        }
    }
});

Vue.component('proxy-details', {
    template: `
<div class="proxy-details">
    <table class="table">
        <tbody>
            <tr>
                <td>Port</td>
                <td>{{proxy.port}}</td>
            </tr>
            <tr>
                <td>Bandwidth Limits</td>
                <td>
                {{infinityIfNegative(proxy.bandwidthLimit.uploadKbps)}}<span class="bandwidth-upload">▲</span>
                 / 
                {{infinityIfNegative(proxy.bandwidthLimit.downloadKbps)}}<span class="bandwidth-download">▼</span>
                </td>
            </tr>
            <tr>
                <td>HAR Logging?</td>
                <td>{{proxy.harLoggingStarted}}</td>
            </tr>                        
        </tbody>
    </table>
</div>    
    `,
    props: ['proxy'],
    methods: {
        infinityIfNegative(number) {
            return number === -1 ? '∞' : number;
        }
    }
});

const app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!?'
    }
});
