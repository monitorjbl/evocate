package io.monitorjbl.evocate.mitm;

import java.util.List;

public interface HttpHeader {
    String name();

    String get();

    List<String> values();

}
