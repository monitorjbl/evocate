package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.session.ProxySession;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpResponseDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MaybeHttpResponseDecoder extends HttpResponseDecoder {
    private static final Logger LOG = LoggerFactory.getLogger(MaybeHttpResponseDecoder.class);
    private final ProxySession proxySession;

    public MaybeHttpResponseDecoder(ProxySession proxySession) {
        this.proxySession = proxySession;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (!proxySession.isWebsocket()) {
            LOG.debug("Decoding HTTP response {}", proxySession, msg.getClass().getName());
            super.channelRead(ctx, msg);
        } else {
            LOG.trace("Not decoding HTTP response {}", proxySession, msg.getClass().getName());
            ctx.fireChannelRead(msg);
        }
    }
}
