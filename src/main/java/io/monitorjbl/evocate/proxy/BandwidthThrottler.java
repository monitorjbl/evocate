package io.monitorjbl.evocate.proxy;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.monitorjbl.evocate.model.Proxy;
import io.monitorjbl.evocate.session.ProxySession;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.EventLoopGroup;
import io.netty.util.concurrent.ScheduledFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

public class BandwidthThrottler {
    private static final Logger LOG = LoggerFactory.getLogger(BandwidthThrottler.class);

    private final Proxy proxy;
    private final EventLoopGroup workerGroup;
    private final AtomicBoolean downloadStarted = new AtomicBoolean(false);
    private final AtomicBoolean uploadStarted = new AtomicBoolean(false);
    private final Queue<Message> downloadQueue = new LinkedBlockingQueue<>();
    private final Queue<Message> uploadQueue = new LinkedBlockingQueue<>();
    private Bucket downloadRateLimiter;
    private Bucket uploadRateLimiter;


    public BandwidthThrottler(Proxy proxy, EventLoopGroup workerGroup) {
        this.proxy = proxy;
        this.workerGroup = workerGroup;
        updateBandwidthLimit();
    }

    public void updateBandwidthLimit() {
        if (proxy.getBandwidthLimit().getUploadKbps() > 0) {
            this.downloadRateLimiter = Bucket4j.builder()
                    .addLimit(Bandwidth.simple(proxy.getBandwidthLimit().getUploadKbps() * 1024, Duration.ofSeconds(1)))
                    .build();
        } else {
            this.downloadRateLimiter = null;
        }

        if (proxy.getBandwidthLimit().getDownloadKbps() > 0) {
            this.uploadRateLimiter = Bucket4j.builder()
                    .addLimit(Bandwidth.simple(proxy.getBandwidthLimit().getDownloadKbps() * 1024, Duration.ofSeconds(1)))
                    .build();
        } else {
            this.uploadRateLimiter = null;
        }
    }

    public void download(ProxySession proxySession, ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (downloadRateLimiter != null) {
            enqueueDownload(proxySession, ctx, msg, promise);
        } else {
            ctx.write(msg, promise);
        }
    }

    public void upload(ProxySession proxySession, ChannelHandlerContext ctx, Object msg) throws Exception {
        if (uploadRateLimiter != null) {
            enqueueUpload(proxySession, ctx, msg);
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    private void enqueueUpload(ProxySession proxySession, ChannelHandlerContext ctx, Object msg) {
        LOG.trace("Enqueuing upload: {}", proxySession, msg.getClass().getName());
        uploadQueue.add(new Message(proxySession, msg, () -> {
            ctx.fireChannelRead(msg);
        }));
        kickstartTask(() -> uploadStarted.get(), () -> {
            uploadStarted.set(true);
            LOG.debug("Starting upload throttler task", proxySession);
            throttledTask("upload", () -> uploadStarted.set(false), uploadRateLimiter, uploadQueue);
        });
    }

    private void enqueueDownload(ProxySession proxySession, ChannelHandlerContext ctx, Object msg, ChannelPromise promise) {
        LOG.trace("Enqueuing download: {}", proxySession, msg.getClass().getName());
        downloadQueue.add(new Message(proxySession, msg, () -> {
            ctx.writeAndFlush(msg, promise);
        }));
        kickstartTask(() -> downloadStarted.get(), () -> {
            downloadStarted.set(true);
            LOG.debug("Starting download throttler task", proxySession);
            throttledTask("download", () -> downloadStarted.set(false), downloadRateLimiter, downloadQueue);
        });
    }

    // This is kinda shitty, it introduces additional delays that are not, strictly speaking, necessary. It should
    // be possible to not have to poll the queue periodically, but instead trigger the next poll after the first
    // poll completes. However, there are some bizarre Netty issues that make this impractical. Namely, the promise
    // for reads/writes is not fulfilled when the request fully completes but is instead fulfilled when the next
    // handler is called.
    private ScheduledFuture throttledTask(String type, Runnable onStop, Bucket rateLimiter, Queue<Message> queue) {
        try {
            if (proxy.isRunning()) {
                var message = queue.poll();
                if (message != null) {
                    LOG.trace("got {} message: {}", message.proxySession, type, message.msg);
                    var proxySession = message.proxySession;
                    var byteBuf = (ByteBuf) message.msg;
                    var size = byteBuf.readableBytes();

                    if (size == 0) {
                        LOG.trace("Firing empty {} message immediately: {}", proxySession, type, message.msg.getClass().getName());
                        message.action.run();
                    } else {
                        var nanos = byteBuf.readableBytes() == 0 ? 0 : rateLimiter.estimateAbilityToConsume(size).getNanosToWaitForRefill();
                        var micros = nanos / 1000;
                        if (micros == 0) {
                            LOG.trace("Firing {} message immediately ({} bytes): {}", proxySession, type, size, message.msg.getClass().getName());
                            rateLimiter.tryConsume(size);
                            message.action.run();
                        } else {
                            LOG.trace("Delaying {} message {} ({} bytes) by {} microseconds", proxySession, type, message.msg.getClass().getName(), size, micros);
                            return workerGroup.schedule(() -> {
                                rateLimiter.tryConsume(size);
                                LOG.trace("Firing {} delayed message ({} bytes): {}", proxySession, type, size, message.msg.getClass().getName());
                                message.action.run();
                                workerGroup.schedule(() -> throttledTask(type, onStop, rateLimiter, queue), 1, TimeUnit.MILLISECONDS);
                            }, micros, TimeUnit.MICROSECONDS);
                        }
                    }
                }
            } else {
                LOG.trace("Proxy is stopped");
                return null;
            }
        } catch (Exception e) {
            LOG.error("Throttler failed on {}!", type, e);
        }

        if (queue.size() > 0) {
            // If there's still stuff in the queue, start processing again immediately
            return throttledTask(type, onStop, rateLimiter, queue);
        } else {
            // If we're here, then the queue is drained and we need to stop looping
            LOG.debug("{} task is out of messages, halting", type);
            onStop.run();
            return null;
        }
    }

    private void kickstartTask(Supplier<Boolean> isStarted, Runnable ifNotStarted) {
        if (!isStarted.get()) {
            ifNotStarted.run();
        }
    }

    private static class Message {
        private final ProxySession proxySession;
        private final Object msg;
        private final Runnable action;

        public Message(ProxySession proxySession, Object msg, Runnable action) {
            this.proxySession = proxySession;
            this.msg = msg;
            this.action = action;
        }
    }
}
