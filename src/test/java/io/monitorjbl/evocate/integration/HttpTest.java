package io.monitorjbl.evocate.integration;

import io.monitorjbl.evocate.BaseTest;
import io.monitorjbl.evocate.model.BandwidthLimit;
import io.monitorjbl.evocate.model.har.HarErrorPhase;
import io.monitorjbl.evocate.model.har.HttpMethod;
import io.monitorjbl.evocate.utils.servers.HttpServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

public class HttpTest extends BaseTest {
    @BeforeAll
    static void startHttp() {
        HttpServer.startServer();
    }

    @AfterAll
    static void stopHttp() {
        HttpServer.stopServer();
    }

    @Test
    void testInsecureHttpForwarding() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy();

        // Send request
        var response = HttpServer.httpRequest(proxy, "/some/endpoint", r -> r.GET().build());

        // Verify reply
        assertNotNull(response);
        assertEquals(0, response.size());
    }

    @Test
    void testInsecureHttpForwardingThrottled() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy(new BandwidthLimit(512, 512));

        // Send request
        var response = HttpServer.httpRequest(proxy, "/some/endpoint", r -> r.GET().build());

        // Verify reply
        assertNotNull(response);
        assertEquals(0, response.size());
    }

    @Test
    void testSecureHttpForwarding() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy();

        // Send request
        var response = HttpServer.httpsRequest(proxy, "/some/endpoint", r -> r.GET().build());

        // Verify reply
        assertNotNull(response);
        assertEquals(0, response.size());
    }

    @Test
    void testSecureHttpForwardingThrottled() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy(new BandwidthLimit(512, 512));

        // Send request
        var response = HttpServer.httpsRequest(proxy, "/some/endpoint", r -> r.GET().build());

        // Verify reply
        assertNotNull(response);
        assertEquals(0, response.size());
    }

    @Test
    void testHttpHarLogging() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy(new BandwidthLimit(512, 512));

        // Send request
        var httpResponse = HttpServer.httpRequest(proxy, "/some/endpoint", r -> r.GET().build());

        // Verify reply
        assertNotNull(httpResponse);
        assertEquals(0, httpResponse.size());

        // Load the HAR log
        var har = EVOCATE_SERVER.getHarLog(proxy);
        assertNotNull(har);
        assertNotNull(har.getLog());
        assertNotNull(har.getLog().getEntries());
        assertEquals(1, har.getLog().getEntries().size());

        // Verify that the log is correct
        var request = har.getLog().getEntries().get(0).getRequest();
        assertEquals(String.format("http://localhost:%s/some/endpoint", HttpServer.getHttpPort()), request.getUrl());

        var response = har.getLog().getEntries().get(0).getResponse();
        assertEquals(200, response.getStatus());
        assertEquals("OK", response.getStatusText());
        assertEquals(response.getContent().getMimeType(), "application/json");
        assertEquals(response.getContent().getText(), OBJECT_MAPPER.writeValueAsString(httpResponse));
    }

    @Test
    void testHttpsHarLogging() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy(new BandwidthLimit(512, 512));

        // Send request
        var httpResponse = HttpServer.httpsRequest(proxy, "/some/endpoint", r -> r.GET().build());

        // Verify reply
        assertNotNull(httpResponse);
        assertEquals(0, httpResponse.size());

        // Load the HAR log
        var har = EVOCATE_SERVER.getHarLog(proxy);
        assertNotNull(har);
        assertNotNull(har.getLog());
        assertNotNull(har.getLog().getEntries());
        assertEquals(1, har.getLog().getEntries().size());

        // Verify that the log is correct
        var request = har.getLog().getEntries().get(0).getRequest();
        assertEquals(String.format("https://localhost:%s/some/endpoint", HttpServer.getHttpsPort()), request.getUrl());

        var response = har.getLog().getEntries().get(0).getResponse();
        assertEquals(200, response.getStatus());
        assertEquals("OK", response.getStatusText());
        assertEquals(response.getContent().getMimeType(), "application/json");
        assertEquals(response.getContent().getText(), OBJECT_MAPPER.writeValueAsString(httpResponse));
    }

    @Test
    void testDisconnectHarLogging() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy(new BandwidthLimit(512, 512));

        // Send request
        try {
            HttpServer.httpRequest(proxy, "/disconnect", r -> r.GET().build());
            fail("should have disconnected and failed");
        } catch (Exception e) {
            // do nothing, failure is expected
        }

        // Load the HAR log
        var har = EVOCATE_SERVER.getHarLog(proxy);
        assertNotNull(har);
        assertNotNull(har.getLog());
        assertNotNull(har.getLog().getEntries());
        assertEquals(1, har.getLog().getEntries().size());

        // Verify that the log is correct
        var request = har.getLog().getEntries().get(0).getRequest();
        var response = har.getLog().getEntries().get(0).getResponse();
        var error = har.getLog().getEntries().get(0).getError();

        assertNotNull(request);
        assertEquals(String.format("http://localhost:%s/disconnect", HttpServer.getHttpPort()), request.getUrl());
        assertEquals(HttpMethod.GET, request.getMethod());

        assertNull(response);

        assertNotNull(error);
        assertEquals(HarErrorPhase.RESPONSE, error.getErrorPhase());
        assertEquals(String.format("localhost:%s", HttpServer.getHttpPort()), error.getRemoteHost());
        assertEquals("Premature disconnect on SERVER_SIDE", error.getErrorMessage());
        assertNull(error.getErrorStacktrace());
    }

}
