package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.model.Proxy;
import io.monitorjbl.evocate.session.ProxySession;
import io.monitorjbl.evocate.session.ProxySessionStateMachine;
import io.monitorjbl.evocate.store.ProxySessionStore;
import io.monitorjbl.evocate.tls.TLSHandlerProvider;
import io.monitorjbl.evocate.EvocateConfig;
import io.monitorjbl.evocate.utils.Utils;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.ByteBufFormat;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadFactory;

public class ProxyChannel {
    private static final Logger LOG = LoggerFactory.getLogger(ProxyChannel.class);

    private final EvocateConfig config;
    private final Proxy proxy;
    private final EventLoopGroup bossGroup;
    private final EventLoopGroup workerGroup;
    private final ProxySessionStore sessionStore = new ProxySessionStore();

    private ChannelFuture channelFuture;
    private boolean running;

    public ProxyChannel(EvocateConfig config, Proxy proxy) {
        this.config = config;
        this.proxy = proxy;
        this.bossGroup = new NioEventLoopGroup(1, threadFactory(String.format("proxy-%s-boss", proxy.getPort())));
        this.workerGroup = new NioEventLoopGroup(config.getProxyThreads(), threadFactory(String.format("proxy-%s-worker", proxy.getPort())));
    }

    public void run() throws Exception {
        try {
            var bootstrap = new ServerBootstrap();
            var throttler = new BandwidthThrottler(proxy, workerGroup);
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, config.getClientConnectionTimeout())
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            // Set up a new proxy session for this request
                            var proxySession = new ProxySession(
                                    ch.id(),
                                    ch,
                                    proxy.getMitm(),
                                    proxy.getBandwidthLimit(),
                                    workerGroup, proxy);
                            sessionStore.put(ch.id(), proxySession);
                            var stateMachine = new ProxySessionStateMachine(config, sessionStore, proxySession, workerGroup);

                            var httpRequestDecoder = new MaybeHttpRequestDecoder(proxySession);
                            var httpResponseEncoder = new MaybeHttpResponseEncoder(proxySession);
                            var websocketEncoder = new MaybeWebsocketEncoder(proxySession, false);
                            var websocketDecoder = new MaybeWebsocketDecoder(proxySession);
                            var tlsServerHandler = TLSHandlerProvider.getServerTLSHandler(config, stateMachine, proxySession);
                            var bandwidthHandler = new BandwidthHandler(proxySession, throttler);
                            var proxyToClientHandler = new ProxyToClientHandler(stateMachine, proxySession, proxy.getMitm());

                            // Register change handler for bandwidth
                            proxy.onBandwidthListenerChange(p -> throttler.updateBandwidthLimit());

                            if (LOG.isTraceEnabled()) {
                                ch.pipeline().addLast("LOGGER", new LoggingHandler(ProxyChannel.class, LogLevel.TRACE, ByteBufFormat.HEX_DUMP));
                            }

                            ch.pipeline().addLast("TLSServerHandler", tlsServerHandler);
                            ch.pipeline().addLast("BandwidthHandler", bandwidthHandler);
                            ch.pipeline().addLast("HttpRequestDecoder", httpRequestDecoder);
                            ch.pipeline().addLast("HttpResponseEncoder", httpResponseEncoder);
                            ch.pipeline().addLast("WebsocketDecoder", websocketDecoder);
                            ch.pipeline().addLast("WebsocketEncoder", websocketEncoder);
                            ch.pipeline().addLast("ProxyToClientHandler", proxyToClientHandler);

                            if (LOG.isTraceEnabled()) {
                                LOG.trace("Proxy-side pipeline: {}", proxySession, Utils.getPipelineDescription(ch));
                            }
                        }

                    }).option(ChannelOption.SO_BACKLOG, 128);

            channelFuture = bootstrap.bind(proxy.getPort()).sync();
            running = true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ProxySessionStore getSessionStore() {
        return sessionStore;
    }

    private ThreadFactory threadFactory(String name) {
        return r -> {
            var t = new Thread(r);
            t.setName(name + "-" + t.getId());
            return t;
        };
    }

    public boolean isRunning() {
        return running;
    }

    public void close() throws Exception {
        if (channelFuture != null) {
            LOG.info("Closing proxy channel");
            running = false;
            channelFuture.channel().close().sync();
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}
