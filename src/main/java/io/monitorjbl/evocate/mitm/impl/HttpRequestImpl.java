package io.monitorjbl.evocate.mitm.impl;

import io.monitorjbl.evocate.mitm.ConnectionInfo;
import io.monitorjbl.evocate.mitm.HttpHeaders;
import io.monitorjbl.evocate.mitm.HttpMethod;
import io.monitorjbl.evocate.mitm.HttpProtocol;
import io.monitorjbl.evocate.mitm.HttpRequest;
import io.monitorjbl.evocate.session.ProxySession;

import javax.annotation.Nonnull;

import static java.util.stream.Collectors.toList;

public class HttpRequestImpl implements HttpRequest {
    private final ConnectionInfo connectionInfo;
    private final HttpProtocol protocol;
    private final HttpMethod method;
    private final String target;
    private final HttpHeaders headers;

    public HttpRequestImpl(@Nonnull ConnectionInfo connectionInfo, @Nonnull HttpProtocol protocol, @Nonnull HttpMethod method,
                           @Nonnull String target, @Nonnull HttpHeaders headers) {
        this.connectionInfo = connectionInfo;
        this.protocol = protocol;
        this.method = method;
        this.target = target;
        this.headers = headers;
    }

    @Override
    public ConnectionInfo connectionInfo() {
        return connectionInfo;
    }

    @Override
    public HttpProtocol protocol() {
        return protocol;
    }

    @Override
    public HttpMethod method() {
        return method;
    }

    @Override
    public String target() {
        return target;
    }

    @Override
    public HttpHeaders headers() {
        return headers;
    }

    @Override
    public String toString() {
        return protocol + " " + method + " " + target + "\n" + headers;
    }

    public static HttpRequest from(ProxySession proxySession, io.netty.handler.codec.http.HttpRequest req) {
        var protocol = HttpProtocol.from(req.protocolVersion());
        var method = HttpMethod.valueOf(req.method().name().toUpperCase());
        var headers = new HttpHeadersImpl(req.headers().names().stream()
                .map(n -> new HttpHeaderImpl(n, req.headers().getAll(n)))
                .collect(toList()));
        var connectionInfo = new ConnectionInfoImpl(proxySession.getTargetHost(), proxySession.getTargetPort(), proxySession.isTLS());
        return new HttpRequestImpl(connectionInfo, protocol, method, req.uri(), headers);
    }
}
