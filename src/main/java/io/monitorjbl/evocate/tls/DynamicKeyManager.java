package io.monitorjbl.evocate.tls;

import com.google.common.io.Resources;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.LogOutputStream;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.X509ExtendedKeyManager;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.Socket;
import java.security.KeyStore;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static io.monitorjbl.evocate.tls.TLSHandlerProvider.KEYSTORE_PASSWORD;
import static io.monitorjbl.evocate.tls.TLSHandlerProvider.KEYSTORE_TYPE;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;

public class DynamicKeyManager extends X509ExtendedKeyManager {
    private static final Logger LOG = LoggerFactory.getLogger(DynamicKeyManager.class);
    private static final File TMP_DIR = new File(".tmp");
    private static final String KEYSTORE_FILE = "evocate.jks";
    private static final Lock CERT_ADD_LOCK = new ReentrantLock();
    private static final Lock KEYSTORE_LOAD_LOCK = new ReentrantLock();
    private static final AtomicBoolean CERT_ADDING = new AtomicBoolean(false);
    private static final Map<String, Boolean> GENERATED_HOSTS = new ConcurrentHashMap<>();
    private static final SecureRandom RANDOM = new SecureRandom();
    private static KeystoreTuple KEYSTORE;
    private static X509ExtendedKeyManager KEY_MANAGER;

    public DynamicKeyManager() { }

    @Override
    public String[] getClientAliases(String keyType, Principal[] issuers) {
        return getManager().getClientAliases(keyType, issuers);
    }

    @Override
    public String chooseClientAlias(String[] keyType, Principal[] issuers, Socket socket) {
        return getManager().chooseClientAlias(keyType, issuers, socket);
    }

    @Override
    public String[] getServerAliases(String keyType, Principal[] issuers) {
        return getManager().getServerAliases(keyType, issuers);
    }

    @Override
    public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {
        return getManager().chooseServerAlias(keyType, issuers, socket);
    }

    @Override
    public X509Certificate[] getCertificateChain(String alias) {
        return getManager().getCertificateChain(alias);
    }

    @Override
    public PrivateKey getPrivateKey(String alias) {
        return getManager().getPrivateKey(alias);
    }

    @Override
    public String chooseEngineClientAlias(String[] keyType, Principal[] issuers, SSLEngine engine) {
        return getManager().chooseEngineClientAlias(keyType, issuers, engine);
    }

    @Override
    public String chooseEngineServerAlias(String keyType, Principal[] issuers, SSLEngine engine) {
        if (engine instanceof DynamicSSLEngine) {
            var hostname = ((DynamicSSLEngine) engine).getHostname();
            var aliases = getServerAliases(keyType, issuers);
            if (aliases == null || aliases.length == 0) {
                return getManager().chooseEngineServerAlias(keyType, issuers, engine);
            }

            for (var alias : aliases) {
                if (alias.equalsIgnoreCase(hostname)) {
                    LOG.info("Selected {} to return", hostname);
                    return alias;
                }
            }
            return aliases[0];
        } else {
            return getManager().chooseEngineServerAlias(keyType, issuers, engine);
        }
    }

    private static X509ExtendedKeyManager getManager() {
        if (KEY_MANAGER == null) {
            loadKeystore();
            KEY_MANAGER = (X509ExtendedKeyManager) KEYSTORE.lazyKeyManager;
        }
        return KEY_MANAGER;
    }

    public static synchronized void addHost(String hostname) {
        if (!GENERATED_HOSTS.containsKey(hostname)) {
            CERT_ADD_LOCK.lock();
            try {
                if (!GENERATED_HOSTS.containsKey(hostname)) {
                    try {
                        CERT_ADDING.set(true);
                        generateCert(hostname);
                        KEYSTORE = null;
                        KEY_MANAGER = null;
                        GENERATED_HOSTS.put(hostname, true);
                    } finally {
                        CERT_ADDING.set(false);
                    }
                }
            } finally {
                CERT_ADD_LOCK.unlock();
            }
        }
    }

    private static void generateCert(String hostname) {
        LOG.info("Generating new cert for {}", hostname);
        var start = System.currentTimeMillis();

        // Create a new temp dir
        var tmpdir = new File(TMP_DIR, "generate-" + hostname);
        tmpdir.mkdirs();

        try {
            // Save the log output here
            var lines = new ArrayList<String>();

            // Run the generate script with the appropriate args
            var rh = new DefaultExecuteResultHandler();
            var exec = new DefaultExecutor();
            exec.setStreamHandler(new PumpStreamHandler(new LogOutputStream() {
                @Override
                protected void processLine(String line, int level) {
                    lines.add(line);
                }
            }));
            exec.setWatchdog(new ExecuteWatchdog(5000));
            exec.setWorkingDirectory(tmpdir);
            exec.execute(
                    new CommandLine(TMP_DIR.getAbsolutePath() + "/generate_on_the_fly_cert.sh")
                            .addArgument(TMP_DIR.getAbsolutePath() + "/evocateCA")
                            .addArgument(TMP_DIR.getAbsolutePath() + "/evocate")
                            .addArgument(hostname),
                    rh);
            rh.waitFor();
            var exitCode = rh.getExitValue();

            if (exitCode != 0) {
                LOG.warn("Failed to generate cert for {}. Log output:\n{}", hostname, lines.stream().collect(joining("\n")));
            } else {
                LOG.info("Cert generation for {} completed in {}ms", hostname, System.currentTimeMillis() - start);
                LOG.debug("Log output:\n{}", lines.stream().collect(joining("\n")));
            }
        } catch (Exception e) {
            LOG.error("Failed to generate new key for {}", hostname, e);
        } finally {
            try {
                FileUtils.deleteDirectory(tmpdir);
            } catch (IOException e) {
                LOG.error("Failed to delete tmp dir at {}", tmpdir.getAbsolutePath(), e);
            }
        }
    }

    static void bootstrap() {
        try {
            if (TMP_DIR.exists()) {
                LOG.info("Keystore already exists, loading keystore domains");
                loadDomains();
            } else {
                // Set up temp dir
                TMP_DIR.mkdirs();

                // Copy out root CA certs
                writeByteArrayToFile(new File(TMP_DIR, "evocateCA.crt"), Resources.toByteArray(Resources.getResource("certs/evocateCA.crt")));
                writeByteArrayToFile(new File(TMP_DIR, "evocateCA.key"), Resources.toByteArray(Resources.getResource("certs/evocateCA.key")));
                writeByteArrayToFile(new File(TMP_DIR, "evocateCA.srl"), Resources.toByteArray(Resources.getResource("certs/evocateCA.srl")));

                // Copy out the JKS keystore
                writeByteArrayToFile(new File(TMP_DIR, KEYSTORE_FILE), Resources.toByteArray(Resources.getResource("certs/evocate.jks")));

                // Create generation script
                var generateScript = new File(TMP_DIR, "generate_on_the_fly_cert.sh");
                writeByteArrayToFile(generateScript, Resources.toByteArray(Resources.getResource("scripts/generate_on_the_fly_cert.sh")));
                generateScript.setExecutable(true);
            }

            loadKeystore();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static void loadKeystore() {
        if (KEYSTORE != null && !CERT_ADDING.get()) {
            return;
        }

        KEYSTORE_LOAD_LOCK.lock();
        try {
            if (KEYSTORE != null) {
                return;
            }

            LOG.info("Loading keystore");
            try (InputStream inputStream = FileUtils.openInputStream(new File(TMP_DIR, KEYSTORE_FILE))) {
                var ks = KeyStore.getInstance("JKS");
                ks = KeyStore.getInstance(KEYSTORE_TYPE);
                ks.load(inputStream, KEYSTORE_PASSWORD.toCharArray());
                KEYSTORE = new KeystoreTuple(ks, new LazyKeyManager(ks, KEYSTORE_PASSWORD.toCharArray()));
                LOG.info("Keystore loaded");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } finally {
            KEYSTORE_LOAD_LOCK.unlock();
        }
    }

    private static void loadDomains() {
        LOG.debug("Loading pre-existing domains");
        try (InputStream inputStream = FileUtils.openInputStream(new File(TMP_DIR, KEYSTORE_FILE))) {
            var ks = KeyStore.getInstance("JKS");
            ks = KeyStore.getInstance(KEYSTORE_TYPE);
            ks.load(inputStream, KEYSTORE_PASSWORD.toCharArray());

            var enumer = ks.aliases();
            while (enumer.hasMoreElements()) {
                var domain = enumer.nextElement();
                LOG.trace("Loaded {}", domain);
                GENERATED_HOSTS.put(domain, true);
            }
            LOG.info("Loaded {} domains", GENERATED_HOSTS.size());
        } catch (Exception e) {
            LOG.error("Failed to load known domains", e);
        }
    }

    private static class KeystoreTuple {
        private final KeyStore keyStore;
        private final LazyKeyManager lazyKeyManager;

        public KeystoreTuple(KeyStore keyStore, LazyKeyManager lazyKeyManager) {
            this.keyStore = keyStore;
            this.lazyKeyManager = lazyKeyManager;
        }
    }
}
