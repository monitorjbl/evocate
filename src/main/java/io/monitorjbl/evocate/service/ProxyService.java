package io.monitorjbl.evocate.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.monitorjbl.evocate.mitm.MITM;
import io.monitorjbl.evocate.mitm.impl.DefaultMITM;
import io.monitorjbl.evocate.mitm.impl.HarMITM;
import io.monitorjbl.evocate.model.BandwidthLimit;
import io.monitorjbl.evocate.model.Proxy;
import io.monitorjbl.evocate.model.har.Har;
import io.monitorjbl.evocate.proxy.ProxyChannel;
import io.monitorjbl.evocate.session.ProxySession;
import io.monitorjbl.evocate.store.ProxyChannelStore;
import io.monitorjbl.evocate.store.ProxyStore;
import io.monitorjbl.evocate.EvocateConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.stream.Collectors.toList;

public class ProxyService {
    private static final Logger LOG = LoggerFactory.getLogger(ProxyService.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final EvocateConfig config;
    private final int startPort;
    private final int endPort;
    private final ProxyStore proxyStore;
    private final ProxyChannelStore proxyChannelStore;
    private final Lock createLock = new ReentrantLock();

    public ProxyService(EvocateConfig config, ProxyStore proxyStore, ProxyChannelStore proxyChannelStore) {
        var s = config.getProxyPortRange().split("-");
        if (s.length != 2) {
            throw new IllegalArgumentException("Illegal port range");
        }
        this.config = config;
        this.startPort = Integer.parseInt(s[0]);
        this.endPort = Integer.parseInt(s[1]);
        this.proxyStore = proxyStore;
        this.proxyChannelStore = proxyChannelStore;
    }

    public List<Proxy> listProxies() {
        return proxyStore.listValues();
    }

    public Optional<Proxy> getProxy(String port) {
        return Optional.ofNullable(proxyStore.get(port));
    }

    public Optional<Har> getProxyHar(String port) {
        MITM mitm = getProxy(port).orElseThrow(() -> new IllegalArgumentException("Proxy not found")).getMitm();
        if (mitm instanceof HarMITM) {
            return Optional.of(((HarMITM) mitm).getHar());
        } else {
            return Optional.empty();
        }
    }

    public Optional<List<String>> getProxySessions(String port) {
        var proxyChannel = proxyChannelStore.get(port);
        if (proxyChannel == null) {
            return Optional.empty();
        }
        return Optional.of(proxyChannel.getSessionStore().listValues().stream()
                .map(s -> String.format("%s::%s - %s", s.getId(), describeConnection(s), s.getServerChannel().getHost()))
                .collect(toList()));
    }

    public Proxy createProxy() {
        int port = startPort;
        try {
            createLock.lock();
            while (port <= endPort) {
                if (!proxyStore.exists(Integer.toString(port))) {
                    break;
                }
                port++;
            }
            if (port > endPort) {
                throw new IllegalStateException("All ports occupied!");
            }
        } finally {
            createLock.unlock();
        }

        var proxy = new Proxy(port);
        var channel = new ProxyChannel(config, proxy);
        proxyStore.put(Integer.toString(proxy.getPort()), proxy);
        proxyChannelStore.put(Integer.toString(proxy.getPort()), channel);

        try {
            proxy.setRunning(true);
            channel.run();
            LOG.info("Created new proxy: {}", OBJECT_MAPPER.writeValueAsString(proxy));
            return proxy;
        } catch (Exception e) {
            LOG.error("Failed to create proxy", e);
            throw new RuntimeException("Failed to create proxy");
        }
    }

    public Proxy startHar(String port) {
        var proxy = getProxy(port).orElseThrow(() -> new IllegalArgumentException("Proxy not found"));
        proxy.setMitm(new HarMITM());
        LOG.info("Started HAR logging on proxy {}", port);
        return proxy;
    }

    public Proxy stopHar(String port) {
        var proxy = getProxy(port).orElseThrow(() -> new IllegalArgumentException("Proxy not found"));
        proxy.setMitm(new DefaultMITM());
        LOG.info("Stopped HAR logging on proxy {}", port);
        return proxy;
    }

    public Proxy setLimits(String port, String uploadKbps, String downloadKbps) {
        var proxy = getProxy(port).orElseThrow(() -> new IllegalArgumentException("Proxy not found"));
        proxy.setBandwidthLimit(getBandwidthLimit(uploadKbps, downloadKbps));
        LOG.info("Set bandwidth limits on proxy {}: {}kbps up/{}kbps down", port, uploadKbps, downloadKbps);
        return proxy;
    }

    public void deleteProxy(String port) {
        var maybeProxy = getProxy(port);
        if (maybeProxy.isPresent()) {
            var proxy = maybeProxy.get();
            var proxyChannel = proxyChannelStore.get(port);
            try {
                proxy.setRunning(false);
                proxyChannel.close();
                proxyChannelStore.remove(port);
                proxyStore.remove(port);
                LOG.info("Deleted proxy on port {}", port);
            } catch (Exception e) {
                LOG.error("Failed to delete proxy", e);
                throw new RuntimeException("Failed to delete proxy");
            }
        } else {
            throw new IllegalArgumentException("Proxy not found");
        }
    }

    public void close() {
        this.proxyStore.listValues().forEach(p -> {
            try {
                proxyChannelStore.get(Integer.toString(p.getPort())).close();
            } catch (Exception e) {
                LOG.error("Unable to shut down proxy {}", p.getPort());
            }
        });
    }

    private static String describeConnection(ProxySession s) {
        if (s.isWebsocket()) {
            return "WS";
        } else if (s.isTLS()) {
            return "HTTPS";
        } else {
            return "HTTP";
        }
    }

    private static BandwidthLimit getBandwidthLimit(String uploadKbps, String downloadKbps) {
        var upload = -1;
        var download = -1;
        if (uploadKbps != null) {
            upload = Integer.parseInt(uploadKbps);
        }
        if (downloadKbps != null) {
            download = Integer.parseInt(downloadKbps);
        }
        return new BandwidthLimit(upload, download);
    }
}
