package io.monitorjbl.evocate.mitm;

import io.netty.handler.codec.http.HttpVersion;

public enum HttpProtocol {
    HTTP_1_0("HTTP", 1, 0), HTTP_1_1("HTTP", 1, 1);

    private final String name;
    private int majorVersion;
    private int minorVersion;

    HttpProtocol(String name, int majorVersion, int minorVersion) {
        this.name = name;
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
    }

    public String getName() {
        return name;
    }

    public int getMajorVersion() {
        return majorVersion;
    }

    public int getMinorVersion() {
        return minorVersion;
    }

    @Override
    public String toString() {
        return name + "/" + majorVersion + "." + minorVersion;
    }

    public static HttpProtocol from(HttpVersion httpVersion) {
        if (httpVersion.protocolName().equals("HTTP")) {
            if (httpVersion.majorVersion() == 1) {
                if (httpVersion.minorVersion() == 0) {
                    return HTTP_1_0;
                }
                if (httpVersion.minorVersion() == 1) {
                    return HTTP_1_1;
                }
            }
        }
        throw new IllegalArgumentException("Unrecognized protocol version: " + httpVersion);
    }
}
