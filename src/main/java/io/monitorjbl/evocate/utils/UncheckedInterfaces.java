package io.monitorjbl.evocate.utils;

public class UncheckedInterfaces {
    @FunctionalInterface
    public interface UncheckedConsumer<T> {
        void accept(T t) throws Exception;
    }

    @FunctionalInterface
    public interface UncheckedSupplier<T> {
        T get() throws Exception;
    }

    @FunctionalInterface
    public interface UncheckedRunnable {
        void run() throws Exception;
    }
}
