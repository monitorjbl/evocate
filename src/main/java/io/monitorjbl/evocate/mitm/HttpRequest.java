package io.monitorjbl.evocate.mitm;

public interface HttpRequest {
    ConnectionInfo connectionInfo();

    HttpProtocol protocol();

    HttpMethod method();

    String target();

    HttpHeaders headers();
}
