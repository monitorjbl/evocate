#!/bin/bash

ROOT_CA=$1
JKS=$2
HOSTNAME=$3
PASSWORD=evocate
KEY_NAME=${HOSTNAME}

# Generate self-signed cert for our site
openssl genrsa -out ${KEY_NAME}.key 2048

# Create a root-signed cert using the self-signed cert
openssl req -new -key ${KEY_NAME}.key -out ${KEY_NAME}.csr -subj "/C=US/ST=NC/L=RTP/O=Evocate/CN=${HOSTNAME}"

# Generate the config needed to merge everything together
echo "authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = ${HOSTNAME}
" > ./${KEY_NAME}.ext

# Merge it all together into one monstrosity-cert
openssl x509 -req -in ${KEY_NAME}.csr -CA ${ROOT_CA}.crt -CAkey ${ROOT_CA}.key -CAcreateserial -out ${KEY_NAME}.crt -days 1825 -sha256 -extfile ${KEY_NAME}.ext -passin pass:${PASSWORD}

# Generate a keystore we can use to serve this key at runtime
openssl pkcs12 -export -in ${KEY_NAME}.crt -inkey ${KEY_NAME}.key -out ${KEY_NAME}.p12 -name "${HOSTNAME}" -passin pass:${PASSWORD} -passout pass:${PASSWORD}
keytool -importkeystore -destkeystore ${JKS}.jks -deststorepass ${PASSWORD} -srckeystore ${KEY_NAME}.p12 -srcstoretype PKCS12 -srcstorepass ${PASSWORD}

# Remove generated files
rm ${HOSTNAME}.crt ${HOSTNAME}.csr ${HOSTNAME}.ext
