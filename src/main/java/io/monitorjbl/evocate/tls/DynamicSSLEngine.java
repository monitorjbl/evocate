package io.monitorjbl.evocate.tls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLEngineResult.HandshakeStatus;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import java.nio.ByteBuffer;

import static java.util.stream.Collectors.joining;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;

public class DynamicSSLEngine extends SSLEngine {
    private static final Logger LOG = LoggerFactory.getLogger(DynamicSSLEngine.class);

    private final DynamicKeyManager keyManager;
    private final SSLEngine sslEngine;
    private String hostname;

    public DynamicSSLEngine(DynamicKeyManager keyManager, SSLEngine sslEngine) {
        this.keyManager = keyManager;
        this.sslEngine = sslEngine;
    }

    public synchronized void setHostname(String hostname) {
        if (this.hostname != null) {
            return;
        }
        DynamicKeyManager.addHost(hostname);
    }

    public String getHostname() {
        return hostname;
    }

    @Override
    public SSLEngineResult wrap(ByteBuffer[] srcs, int offset, int length, ByteBuffer dst) throws SSLException {
        return getSslEngine().wrap(srcs, offset, length, dst);
    }

    @Override
    public SSLEngineResult unwrap(ByteBuffer src, ByteBuffer[] dsts, int offset, int length) throws SSLException {
        return getSslEngine().unwrap(src, dsts, offset, length);
    }

    @Override
    public Runnable getDelegatedTask() {
        return getSslEngine().getDelegatedTask();
    }

    @Override
    public void closeInbound() throws SSLException {
        if (sslEngine == null) {
            return;
        }
        getSslEngine().closeInbound();
    }

    @Override
    public boolean isInboundDone() {
        return getSslEngine().isInboundDone();
    }

    @Override
    public void closeOutbound() {
        if (sslEngine == null) {
            return;
        }
        getSslEngine().closeOutbound();
    }

    @Override
    public boolean isOutboundDone() {
        return getSslEngine().isOutboundDone();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return getSslEngine().getSupportedCipherSuites();
    }

    @Override
    public String[] getEnabledCipherSuites() {
        return getSslEngine().getEnabledCipherSuites();
    }

    @Override
    public void setEnabledCipherSuites(String[] suites) {
        getSslEngine().setEnabledCipherSuites(suites);
    }

    @Override
    public String[] getSupportedProtocols() {
        return getSslEngine().getSupportedProtocols();
    }

    @Override
    public String[] getEnabledProtocols() {
        return getSslEngine().getEnabledProtocols();
    }

    @Override
    public void setEnabledProtocols(String[] protocols) {
        getSslEngine().setEnabledProtocols(protocols);
    }

    @Override
    public SSLSession getSession() {
        return getSslEngine().getSession();
    }

    @Override
    public void beginHandshake() throws SSLException {
        getSslEngine().beginHandshake();
    }

    @Override
    public HandshakeStatus getHandshakeStatus() {
        return getSslEngine().getHandshakeStatus();
    }

    @Override
    public void setUseClientMode(boolean mode) {
        getSslEngine().setUseClientMode(mode);
    }

    @Override
    public boolean getUseClientMode() {
        return false;
    }

    @Override
    public void setNeedClientAuth(boolean need) {
        getSslEngine().setNeedClientAuth(need);
    }

    @Override
    public boolean getNeedClientAuth() {
        return false;
    }

    @Override
    public void setWantClientAuth(boolean want) {
        getSslEngine().setWantClientAuth(want);
    }

    @Override
    public boolean getWantClientAuth() {
        return getSslEngine().getWantClientAuth();
    }

    @Override
    public void setEnableSessionCreation(boolean flag) {
        getSslEngine().setEnableSessionCreation(flag);
    }

    @Override
    public boolean getEnableSessionCreation() {
        return getSslEngine().getEnableSessionCreation();
    }

    private SSLEngine getSslEngine() {
        if (sslEngine == null) {
            throw new IllegalStateException("SSLEngine is not ready yet");
        }
        return sslEngine;
    }

}
