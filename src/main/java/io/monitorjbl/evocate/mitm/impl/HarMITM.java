package io.monitorjbl.evocate.mitm.impl;

import io.monitorjbl.evocate.mitm.EventSource;
import io.monitorjbl.evocate.mitm.HttpRequest;
import io.monitorjbl.evocate.mitm.HttpResponse;
import io.monitorjbl.evocate.mitm.MITM;
import io.monitorjbl.evocate.model.har.Har;
import io.monitorjbl.evocate.model.har.HarCreatorBrowser;
import io.monitorjbl.evocate.model.har.HarEntry;
import io.monitorjbl.evocate.model.har.HarError;
import io.monitorjbl.evocate.model.har.HarErrorPhase;
import io.monitorjbl.evocate.model.har.HarHeader;
import io.monitorjbl.evocate.model.har.HarLog;
import io.monitorjbl.evocate.model.har.HarPage;
import io.monitorjbl.evocate.model.har.HarQueryParam;
import io.monitorjbl.evocate.model.har.HarRequest;
import io.monitorjbl.evocate.model.har.HarResponse;
import io.monitorjbl.evocate.model.har.HarTiming;
import io.monitorjbl.evocate.model.har.HttpMethod;
import io.netty.handler.codec.http.HttpHeaderNames;
import org.brotli.dec.BrotliInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class HarMITM implements MITM {
    private static final Logger LOG = LoggerFactory.getLogger(HarMITM.class);
    private static final int DEFAULT_MAX_STORED_CONTENT = 16384;
    private static final String DEFAULT_CONTENT_TYPE = "application/octet-stream";
    private static final String PAGE_ID = "page_0";
    private static final String REMOTE_HOST_ATTR = "_remoteHost";
    private static final String REMOTE_PORT_ATTR = "_remotePort";

    private final Har har = new Har();
    private final Map<String, HarEntry> liveRequests = new ConcurrentHashMap<>();
    private final Map<String, TimingTracker> timingsTrackers = new ConcurrentHashMap<>();
    private final Map<String, TrackedBufferedOutputStream> requestData = new ConcurrentHashMap<>();
    private final Map<String, TrackedBufferedOutputStream> responseData = new ConcurrentHashMap<>();

    private final int maxStoredContent;

    public HarMITM() {
        this(DEFAULT_MAX_STORED_CONTENT);
    }

    public HarMITM(int maxStoredContent) {
        this.maxStoredContent = maxStoredContent;
        this.har.setLog(new HarLog());
        this.har.getLog().setCreator(new HarCreatorBrowser());
        this.har.getLog().getCreator().setName("Evocate HAR Log");
        this.har.getLog().getCreator().setVersion("1.0");

        var page = new HarPage();
        page.setId(PAGE_ID);
        page.setTitle("Page 0");
        page.setStartedDateTime(new Date());
        this.har.getLog().setPages(asList(page));
    }

    public Har getHar() {
        return har;
    }

    @Override
    public void onConnectStart(String channelId, String host, int port) {
        LOG.debug("onConnectStart: {} -> {}:{} ({})", channelId, host, port, new Date());
        var entry = new HarEntry();
        var tracker = new TimingTracker();
        entry.setStartedDateTime(new Date());
        entry.setPageref(PAGE_ID);
        entry.setTimings(new HarTiming());
        entry.setConnection(channelId);
        entry.getAdditional().put(REMOTE_HOST_ATTR, host);
        entry.getAdditional().put(REMOTE_PORT_ATTR, port);
        tracker.connectStart = System.nanoTime();

        liveRequests.put(channelId, entry);
        timingsTrackers.put(channelId, tracker);
    }

    @Override
    public void onConnectComplete(String channelId) {
        LOG.debug("onConnectComplete: {}", channelId);
        if (!liveRequests.containsKey(channelId)) {
            LOG.warn("No idea how we got into this state, the connect ended before it started!");
            return;
        }

        timingsTrackers.get(channelId).connectEnd = System.nanoTime();
    }

    @Override
    public void onTLSStart(String channelId) {
        LOG.debug("onTLSStart: {}", channelId);
        timingsTrackers.get(channelId).tlsStart = System.nanoTime();
    }

    @Override
    public void onTLSComplete(String channelId) {
        LOG.debug("onTLSComplete: {}", channelId);
        timingsTrackers.get(channelId).tlsEnd = System.nanoTime();
    }

    @Override
    public void onRequestStart(String channelId, HttpRequest request) {
        LOG.debug("onRequestStart: {} -> {} {}", channelId, request.method(), request.target());
        if (!liveRequests.containsKey(channelId)) {
            LOG.warn("No idea how we got into this state, the request started before it connected!");
            return;
        }

        timingsTrackers.get(channelId).requestStart = System.nanoTime();
        var entry = liveRequests.get(channelId);

        var req = new HarRequest();
        req.setHttpVersion(request.protocol().toString());
        req.setMethod(HttpMethod.valueOf(request.method().name()));
        req.setHeaders(request.headers().names().stream()
                .map(n -> toHeader(n, request.headers().get(n)))
                .collect(toList()));
        req.setQueryString(extractQueryParams(request.target()));
        req.setBodySize(0L);
        req.setHeadersSize(req.getHeaders().stream()
                .mapToLong(h -> h.getName().length() + h.getValue().length())
                .sum());

        // Determine the full URL from the connection info and the request path
        String protocol;
        if (request.headers().get("Upgrade") != null) {
            protocol = request.connectionInfo().isTls() ? "wss://" : "ws://";
        } else {
            protocol = request.connectionInfo().isTls() ? "https://" : "http://";
        }

        // Include the port only if it is non-standard
        if (request.connectionInfo().port() != 80 && request.connectionInfo().port() != 443) {
            req.setUrl(protocol + request.connectionInfo().host() + ":" + request.connectionInfo().port() + request.target());
        } else {
            req.setUrl(protocol + request.connectionInfo().host() + request.target());
        }

        entry.setRequest(req);
        liveRequests.put(channelId, entry);
    }

    @Override
    public void onRequestData(String channelId, byte[] data) {
        LOG.debug("onRequestData: {}", channelId);
        if (!liveRequests.containsKey(channelId)) {
            LOG.warn("No idea how we got into this state, the request sent data before it started!");
            return;
        }
        var entry = liveRequests.get(channelId);

        if (!requestData.containsKey(channelId)) {
            requestData.put(channelId, new TrackedByteArrayOutputStream());
        }
        var buff = requestData.get(channelId);

        try {
            entry.getRequest().setBodySize(entry.getRequest().getBodySize() + data.length);

            // Make sure we don't store more data than we're supposed to
            if (buff.getCount() >= maxStoredContent) {
                return;
            }
            if (buff.getCount() + data.length > maxStoredContent) {
                data = Arrays.copyOfRange(data, 0, (maxStoredContent - buff.getCount()));
            }
            buff.write(data);
        } catch (IOException e) {
            LOG.error("Failed to write request data (somehow)", e);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void onRequestComplete(String channelId) {
        LOG.debug("onRequestComplete: {}", channelId);
        if (!liveRequests.containsKey(channelId)) {
            LOG.warn("No idea how we got into this state, the request ended before it started!");
            return;
        }
        timingsTrackers.get(channelId).requestEnd = System.nanoTime();
        var entry = liveRequests.get(channelId);

        try {
            if (requestData.containsKey(channelId)) {
                var buf = requestData.get(channelId);
                var req = entry.getRequest();

                req.getPostData().setMimeType(getMimeType(req.getHeaders()));
                req.getPostData().setText(maybeEncode(buf.getContent(), req.getPostData().getMimeType()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            requestData.remove(channelId);
        }
    }

    @Override
    public void onResponseStart(String channelId, HttpResponse response) {
        LOG.debug("onResponseStart: {}", channelId);
        if (!liveRequests.containsKey(channelId)) {
            LOG.warn("No idea how we got into this state, the response started before the request!");
            return;
        }
        timingsTrackers.get(channelId).responseStart = System.nanoTime();
        var entry = liveRequests.get(channelId);

        var resp = new HarResponse();
        resp.setHttpVersion(response.protocol().toString());
        resp.setStatus(response.statusCode());
        resp.setStatusText(response.statusText());
        resp.setHeaders(response.headers().names().stream()
                .map(n -> toHeader(n, response.headers().get(n)))
                .collect(toList()));

        var compression = response.headers().names().stream()
                .filter(n -> n.equals("Content-Encoding"))
                .map(n -> {
                    if (response.headers().get(n).toLowerCase().contains("gzip")) {
                        return "gzip";
                    } else if (response.headers().get(n).toLowerCase().contains("br")) {
                        return "br";
                    } else {
                        return null;
                    }
                })
                .findFirst();
        resp.setCompression(compression.orElse(null));

        var redirectUrl = response.headers().get("Location");
        if (response.statusCode() == 301 && redirectUrl != null) {
            resp.setRedirectURL(redirectUrl);
        }

        //TODO: Source this from the actual response
        resp.setHeadersSize(resp.getHeaders().stream()
                .mapToLong(h -> h.getName().length() + h.getValue().length())
                .sum());

        entry.setResponse(resp);
    }

    @Override
    public void onResponseData(String channelId, byte[] data) {
        LOG.debug("onResponseData: {}", channelId);
        if (!liveRequests.containsKey(channelId)) {
            LOG.warn("No idea how we got into this state, the response sent data before the request started!");
            return;
        }
        var entry = liveRequests.get(channelId);

        if (!responseData.containsKey(channelId)) {
            // If the response is compressed, we need to wrap the output stream in an inflater
            if ("gzip".equals(entry.getResponse().getCompression())) {
                responseData.put(channelId, new TrackedGzipByteArrayOutputStream());
            } else if ("br".equals(entry.getResponse().getCompression())) {
                responseData.put(channelId, new TrackedBrotliByteArrayOutputStream());
            } else {
                responseData.put(channelId, new TrackedByteArrayOutputStream());
            }
        }
        var buff = responseData.get(channelId);

        try {
            entry.getResponse().setBodySize(entry.getResponse().getBodySize() + data.length);

            // Make sure we don't store more data than we're supposed to. The exception to this rule is if the data
            // being sent is compressed. We can't decompress the data until the complete stream is received, so we'll
            // buffer it in memory completely.
            if (entry.getResponse().getCompression() == null && buff.getCount() >= maxStoredContent) {
                return;
            }
            if (entry.getResponse().getCompression() == null && buff.getCount() + data.length > maxStoredContent) {
                data = Arrays.copyOfRange(data, 0, (maxStoredContent - buff.getCount()));
            }
            buff.write(data);
        } catch (IOException e) {
            LOG.error("Failed to write response data (somehow)", e);
        }
    }

    @Override
    public void onResponseComplete(String channelId) {
        LOG.debug("onResponseComplete: {}", channelId);
        if (!liveRequests.containsKey(channelId)) {
            LOG.warn("No idea how we got into this state, the response ended before the request!");
            return;
        }
        timingsTrackers.get(channelId).responseEnd = System.nanoTime();
        var entry = liveRequests.get(channelId);

        try {
            if (responseData.containsKey(channelId)) {
                var buf = responseData.get(channelId);
                var res = entry.getResponse();

                // Detect compressed data so we can decompress it. The fully-buffered data will be
                // returned, so make sure we truncate it before storing the response body.
                var body = buf.getContent();
                var bodyLength = body.length;
                if (res.getCompression() != null) {
                    if (body.length > maxStoredContent) {
                        body = Arrays.copyOfRange(body, 0, maxStoredContent);
                    }
                }

                res.getContent().setSize((long) bodyLength);
                res.getContent().setMimeType(getMimeType(res.getHeaders()));
                res.getContent().setText(maybeEncode(body, res.getContent().getMimeType()));
            }

            // Set the timings for this connection
            var timings = entry.getTimings();
            var tracker = timingsTrackers.get(channelId);
            entry.setTime((int) ((tracker.responseEnd - tracker.connectStart) / 1_000_000));
            timings.setConnect(tracker.connectEnd - tracker.connectStart, TimeUnit.NANOSECONDS);
            timings.setSsl(tracker.tlsEnd - tracker.tlsEnd, TimeUnit.NANOSECONDS);
            timings.setSend(tracker.requestEnd - tracker.requestStart, TimeUnit.NANOSECONDS);
            timings.setWait(tracker.responseStart - tracker.requestEnd, TimeUnit.NANOSECONDS);
            timings.setReceive(tracker.responseEnd - tracker.responseStart, TimeUnit.NANOSECONDS);
        } finally {
            cleanup(entry);
            har.getLog().getEntries().add(entry);
        }
    }

    @Override
    public void onDisconnect(String channelId, EventSource errorSource) {
        // This state is the happy path. We expect the live request to be cleaned up before the channel
        // is disconnected. Do nothing if the request is not present.
        if (!liveRequests.containsKey(channelId)) {
            return;
        }

        var entry = liveRequests.get(channelId);
        var timing = timingsTrackers.get(channelId);

        try {
            entry.setError(generateError(entry, timing, "Premature disconnect on " + errorSource));
        } finally {
            cleanup(entry);
            har.getLog().getEntries().add(entry);
        }
    }

    @Override
    public void onError(String channelId, EventSource errorSource, Throwable cause) {
        LOG.debug("onError: {}", channelId);
        if (!liveRequests.containsKey(channelId)) {
            LOG.warn("No idea how we got into this state, the channel errored out before the request started!");
            return;
        }

        var entry = liveRequests.get(channelId);
        var timing = timingsTrackers.get(channelId);

        try {
            var error = generateError(entry, timing, "Error encountered on " + errorSource);

            // Log the error message as the full stacktrace
            var sw = new StringWriter();
            var pw = new PrintWriter(sw);
            cause.printStackTrace(pw);
            error.setErrorStacktrace(sw.toString());

            entry.setError(error);
        } finally {
            cleanup(entry);
            har.getLog().getEntries().add(entry);
        }
    }

    private void cleanup(HarEntry entry) {
        entry.getAdditional().remove(REMOTE_HOST_ATTR);
        entry.getAdditional().remove(REMOTE_PORT_ATTR);
        responseData.remove(entry.getConnection());
        liveRequests.remove(entry.getConnection());
        timingsTrackers.remove(entry.getConnection());
    }

    private static HarError generateError(HarEntry entry, TimingTracker timing, String errorMessage) {
        var error = new HarError();
        error.setErrorPhase(determineErrorPhase(entry, timing));
        error.setErrorMessage(errorMessage);
        error.setRemoteHost(String.format("%s:%s", entry.getAdditional().get(REMOTE_HOST_ATTR), entry.getAdditional().get(REMOTE_PORT_ATTR)));
        return error;
    }

    private static HarErrorPhase determineErrorPhase(HarEntry entry, TimingTracker timing) {
        if (timing.connectEnd < 0) {
            return HarErrorPhase.CONNECTION;
        } else if (timing.tlsEnd < 0 && isTls(entry.getRequest())) {
            return HarErrorPhase.TLS_HANDSHAKE;
        } else if (timing.requestEnd < 0) {
            return HarErrorPhase.REQUEST;
        } else {
            return HarErrorPhase.RESPONSE;
        }
    }

    private static String maybeEncode(byte[] data, String mimeType) {
        if (isTextData(mimeType)) {
            return new String(data);
        } else {
            return new String(Base64.getEncoder().encode(data));
        }
    }

    private static boolean isTls(HarRequest request) {
        return request != null && (request.getUrl().startsWith("https://") || request.getUrl().startsWith("wss://"));
    }

    private static boolean isTextData(String mimeType) {
        return mimeType != null
                && (mimeType.contains("text/")
                || mimeType.contains("application/json")
                || mimeType.contains("application/ld+json")
                || mimeType.contains("application/xml")
                || mimeType.contains("application/xhtml"));
    }

    private static HarHeader toHeader(String k, String v) {
        var h = new HarHeader();
        h.setName(k);
        h.setValue(v);
        return h;
    }

    private static List<HarQueryParam> extractQueryParams(String path) {
        var firstSplit = path.split("\\?");
        if (firstSplit.length > 1) {
            return Stream.of(firstSplit[1].split("\\&")).map(s -> {
                var split = s.split("=");
                if (split.length > 1) {
                    var param = new HarQueryParam();
                    param.setName(split[0]);
                    param.setValue(split[1]);
                    return param;
                } else {
                    return null;
                }
            }).filter(Objects::nonNull).collect(toList());
        } else {
            return emptyList();
        }
    }

    private static String getMimeType(List<HarHeader> headers) {
        return headers.stream()
                .filter(h -> h.getName().equalsIgnoreCase(HttpHeaderNames.CONTENT_TYPE.toString()))
                .map(h -> h.getValue())
                .findFirst()
                .orElseGet(() -> {
                    LOG.debug("No content type specified. Content will be treated as {}", DEFAULT_CONTENT_TYPE);
                    return DEFAULT_CONTENT_TYPE;
                });
    }

    private interface TrackedBufferedOutputStream {
        int getCount();

        byte[] getContent();

        void write(byte[] content) throws IOException;
    }

    private static class TrackedBrotliByteArrayOutputStream implements TrackedBufferedOutputStream {
        private final TrackedByteArrayOutputStream source = new TrackedByteArrayOutputStream();

        @Override
        public int getCount() {
            return source.getCount();
        }

        @Override
        public byte[] getContent() {
            try {
                return new BrotliInputStream(new ByteArrayInputStream(source.getContent())).readAllBytes();
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }

        @Override
        public void write(byte[] content) throws IOException {
            source.write(content);
        }
    }

    private static class TrackedGzipByteArrayOutputStream implements TrackedBufferedOutputStream {
        private final TrackedByteArrayOutputStream source = new TrackedByteArrayOutputStream();

        @Override
        public int getCount() {
            return source.getCount();
        }

        @Override
        public byte[] getContent() {
            try {
                return new GZIPInputStream(new ByteArrayInputStream(source.getContent())).readAllBytes();
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }

        @Override
        public void write(byte[] content) throws IOException {
            source.write(content);
        }
    }

    private static class TrackedByteArrayOutputStream extends ByteArrayOutputStream implements TrackedBufferedOutputStream {
        @Override
        public int getCount() {
            return super.count;
        }

        @Override
        public byte[] getContent() {
            return super.toByteArray();
        }
    }

    private static class TimingTracker {
        private long connectStart = -1;
        private long connectEnd = -1;
        private long tlsStart = -1;
        private long tlsEnd = -1;
        private long requestStart = -1;
        private long requestEnd = -1;
        private long responseStart = -1;
        private long responseEnd = -1;
    }
}
