package io.monitorjbl.evocate.integration;

import io.monitorjbl.evocate.BaseTest;
import io.monitorjbl.evocate.model.BandwidthLimit;
import io.monitorjbl.evocate.utils.servers.WebsocketServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

public class WebsocketTest extends BaseTest {
    @BeforeAll
    static void startWebsocket() {
        WebsocketServer.startServer();
    }

    @AfterAll
    static void stopWebsocket() {
        WebsocketServer.stopServer();
    }

    @Test
    void testInsecureWebsocketForwarding() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy();
        var client = WebsocketServer.getWSClient(proxy);
        client.connect();
        while (!client.isOpen()) {
            Thread.sleep(10);
        }

        // Send request
        var received = new AtomicBoolean();
        var msg = new String[]{null};
        client.sendMessage("Yeet", m -> {
            received.set(true);
            msg[0] = m;
        });
        while (!received.get() && client.isOpen()) {
            Thread.sleep(10);
        }

        // Verify reply
        Assertions.assertNotNull(msg[0]);
        Assertions.assertEquals("Yeet", msg[0]);

        // Disconnect client
        client.close();
    }

    @Test
    void testInsecureWebsocketForwardingThrottled() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy(new BandwidthLimit(512, 512));
        var client = WebsocketServer.getWSClient(proxy);
        client.connect();
        while (!client.isOpen()) {
            Thread.sleep(10);
        }

        // Send request
        var received = new AtomicBoolean();
        var msg = new String[]{null};
        client.sendMessage("Yeet", m -> {
            received.set(true);
            msg[0] = m;
        });
        while (!received.get() && client.isOpen()) {
            Thread.sleep(10);
        }

        // Verify reply
        Assertions.assertNotNull(msg[0]);
        Assertions.assertEquals("Yeet", msg[0]);

        // Disconnect client
        client.close();
    }

    @Test
    void testSecureWebsocketForwarding() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy();
        var client = WebsocketServer.getWSSClient(proxy);
        client.connect();
        while (!client.isOpen()) {
            Thread.sleep(10);
        }

        // Send request
        var received = new AtomicBoolean();
        var msg = new String[]{null};
        client.sendMessage("Yeet", m -> {
            received.set(true);
            msg[0] = m;
        });
        while (!received.get() && client.isOpen()) {
            Thread.sleep(10);
        }

        // Verify reply
        Assertions.assertNotNull(msg[0]);
        Assertions.assertEquals("Yeet", msg[0]);

        // Disconnect client
        client.close();
    }

    @Test
    void testSecureWebsocketForwardingThrottled() throws Exception {
        var proxy = EVOCATE_SERVER.createNewProxy(new BandwidthLimit(512, 512));
        var client = WebsocketServer.getWSSClient(proxy);
        client.connect();
        while (!client.isOpen()) {
            Thread.sleep(10);
        }

        // Send request
        var received = new AtomicBoolean();
        var msg = new String[]{null};
        client.sendMessage("Yeet", m -> {
            received.set(true);
            msg[0] = m;
        });
        while (!received.get() && client.isOpen()) {
            Thread.sleep(10);
        }

        // Verify reply
        Assertions.assertNotNull(msg[0]);
        Assertions.assertEquals("Yeet", msg[0]);

        // Disconnect client
        client.close();
    }
}
