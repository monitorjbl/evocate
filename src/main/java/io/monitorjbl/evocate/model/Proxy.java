package io.monitorjbl.evocate.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.monitorjbl.evocate.mitm.MITM;
import io.monitorjbl.evocate.mitm.impl.DefaultMITM;
import io.monitorjbl.evocate.mitm.impl.HarMITM;

import java.util.function.Consumer;

public class Proxy implements Comparable {
    private final int port;

    private BandwidthLimit bandwidthLimit = new BandwidthLimit(-1, -1);
    @JsonIgnore
    private MITM mitm = new DefaultMITM();
    private boolean isRunning;
    private Consumer<Proxy> bandwidthLimitChangeListeners;

    public Proxy(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public BandwidthLimit getBandwidthLimit() {
        return bandwidthLimit;
    }

    public void setBandwidthLimit(BandwidthLimit bandwidthLimit) {
        this.bandwidthLimit = bandwidthLimit;
        if (bandwidthLimitChangeListeners != null) {
            bandwidthLimitChangeListeners.accept(this);
        }
    }

    public void onBandwidthListenerChange(Consumer<Proxy> func) {
        this.bandwidthLimitChangeListeners = func;
    }

    public MITM getMitm() {
        return mitm;
    }

    public void setMitm(MITM mitm) {
        this.mitm = mitm;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    @JsonProperty
    public boolean isHarLoggingStarted() {
        return mitm instanceof HarMITM;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Proxy) {
            return Integer.compare(getPort(), ((Proxy) o).getPort());
        }
        return 0;
    }
}
