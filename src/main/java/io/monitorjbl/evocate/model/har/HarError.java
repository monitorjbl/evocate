package io.monitorjbl.evocate.model.har;

public class HarError {
    private HarErrorPhase errorPhase;
    private String errorMessage;
    private String errorStacktrace;
    private String remoteHost;

    public HarErrorPhase getErrorPhase() {
        return errorPhase;
    }

    public void setErrorPhase(HarErrorPhase errorPhase) {
        this.errorPhase = errorPhase;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorStacktrace() {
        return errorStacktrace;
    }

    public void setErrorStacktrace(String errorStacktrace) {
        this.errorStacktrace = errorStacktrace;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }
}
