package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.session.ProxySession;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

public class BandwidthHandler extends ChannelDuplexHandler {
    private final ProxySession proxySession;
    private final BandwidthThrottler throttler;

    public BandwidthHandler(ProxySession proxySession, BandwidthThrottler throttler) {
        this.proxySession = proxySession;
        this.throttler = throttler;
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        throttler.download(proxySession, ctx, msg, promise);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        throttler.upload(proxySession, ctx, msg);
    }
}
