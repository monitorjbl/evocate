package io.monitorjbl.evocate.mitm;

public interface ConnectionInfo {
    String host();

    int port();

    boolean isTls();
}
