package io.monitorjbl.evocate.model;

public class BandwidthLimit {
    private final int uploadKbps;
    private final int downloadKbps;

    public BandwidthLimit(int uploadKbps, int downloadKbps) {
        this.uploadKbps = uploadKbps;
        this.downloadKbps = downloadKbps;
    }

    public int getUploadKbps() {
        return uploadKbps;
    }

    public int getDownloadKbps() {
        return downloadKbps;
    }
}
