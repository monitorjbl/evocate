package io.monitorjbl.evocate;

import ch.qos.logback.classic.Level;
import io.monitorjbl.evocate.rest.EvocateRestServer;
import io.monitorjbl.evocate.tls.TLSHandlerProvider;
import io.monitorjbl.evocate.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EvocateApp implements Runnable, AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(EvocateApp.class);

    private final EvocateConfig config;
    private EvocateRestServer restServer;

    public EvocateApp(EvocateConfig config) {
        this.config = config;
    }

    public synchronized void start() {
        if (restServer != null) {
            throw new IllegalArgumentException("Server is already started");
        }

        System.out.println("\n" +
                "▄▄▄ . ▌ ▐·       ▄▄·  ▄▄▄· ▄▄▄▄▄▄▄▄ .\n" +
                "▀▄.▀·▪█·█▌▪     ▐█ ▌▪▐█ ▀█ •██  ▀▄.▀·\n" +
                "▐▀▀▪▄▐█▐█• ▄█▀▄ ██ ▄▄▄█▀▀█  ▐█.▪▐▀▀▪▄\n" +
                "▐█▄▄▌ ███ ▐█▌.▐▌▐███▌▐█ ▪▐▌ ▐█▌·▐█▄▄▌\n" +
                " ▀▀▀ . ▀   ▀█▄▀▪·▀▀▀  ▀  ▀  ▀▀▀  ▀▀▀ \n" +
                "\nStartup configuration:\n    " +
                Utils.dumpAsYAML(config)
                        .replaceAll("---\n", "")
                        .replaceAll("\n", "\n    "));

        // Configure logging output
        configureLogging();

        // Do some static initialization
        TLSHandlerProvider.bootstrap(config);

        try {
            restServer = new EvocateRestServer(config);
            restServer.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void waitFor() {
        if (restServer == null) {
            throw new IllegalStateException("Server is not started");
        }

        try {
            synchronized (restServer) {
                restServer.wait();
            }
            LOG.info("Shutting down");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        start();
        waitFor();
    }

    @Override
    public synchronized void close() {
        if (restServer == null) {
            throw new IllegalStateException("Server is not started");
        }

        try {
            synchronized (restServer) {
                restServer.stop();
                restServer.notify();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void configureLogging() {
        var evocateLogger = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger("io.monitorjbl.evocate");
        var vertxLogger = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger("io.vertx");
        var nettyLogger = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger("io.netty");
        if (config.getTraceLogging()) {
            evocateLogger.setLevel(Level.TRACE);
            vertxLogger.setLevel(Level.TRACE);
            nettyLogger.setLevel(Level.TRACE);
        } else if (config.getDebugLogging()) {
            evocateLogger.setLevel(Level.DEBUG);
            vertxLogger.setLevel(Level.WARN);
            nettyLogger.setLevel(Level.WARN);
        } else {
            evocateLogger.setLevel(Level.INFO);
            vertxLogger.setLevel(Level.WARN);
            nettyLogger.setLevel(Level.WARN);
        }
    }

    public static void main(String[] args) throws Exception {
        new EvocateApp(EvocateConfig.parseArgs(cli -> {
            cli.setUsageHelpWidth(120);
            cli.setCommandName("evocate");
            if (cli.isUsageHelpRequested()) {
                cli.usage(System.out);
                System.exit(0);
            } else if (cli.isVersionHelpRequested()) {
                cli.printVersionHelp(System.out);
                System.exit(0);
            }
        }, args)).run();
    }
}
