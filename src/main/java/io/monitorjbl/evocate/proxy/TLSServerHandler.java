package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.model.ProxySessionState;
import io.monitorjbl.evocate.session.ProxySession;
import io.monitorjbl.evocate.session.ProxySessionStateMachine;
import io.monitorjbl.evocate.tls.DynamicSSLEngine;
import io.monitorjbl.evocate.EvocateConfig;
import io.monitorjbl.evocate.utils.UncheckedInterfaces.UncheckedRunnable;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.ReferenceCountUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLEngine;

import static io.monitorjbl.evocate.utils.Utils.logByteBufContent;

public class TLSServerHandler extends SslHandler {
    private static final Logger LOG = LoggerFactory.getLogger(TLSServerHandler.class);

    private final EvocateConfig config;
    private final SSLEngine engine;
    private final ProxySessionStateMachine stateMachine;
    private final ProxySession proxySession;

    public TLSServerHandler(EvocateConfig config, SSLEngine engine, ProxySessionStateMachine stateMachine, ProxySession proxySession) {
        super(engine);
        this.config = config;
        this.engine = engine;
        this.stateMachine = stateMachine;
        this.proxySession = proxySession;
        setHandshakeTimeoutMillis(config.getRemoteTLSHandshakeTimeout());
    }

    @Override
    public void flush(ChannelHandlerContext ctx) throws Exception {
        if (engine instanceof DynamicSSLEngine) {
            ((DynamicSSLEngine) engine).setHostname(proxySession.getTargetHost());
        }
        super.flush(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ifTLS(
                () -> {
                    LOG.trace("TLS read {}", proxySession, logByteBufContent(LOG, msg));
                    super.channelRead(ctx, msg);
                },
                () -> {
                    if (proxySession.isInState(ProxySessionState.AWAITING_CONNECTION) && msg instanceof ByteBuf) {
                        // Copy out the message because buffers can only be read once
                        var copy = ((ByteBuf) msg).copy();
                        var bytes = new byte[copy.readableBytes()];
                        copy.readBytes(bytes);
                        ReferenceCountUtil.release(copy);

                        // TLS handshake payloads start with 0x16 (decimal 22) in the first byte.
                        if (bytes[0] == 0x16) {
                            LOG.trace("Detected TLS client request, switching session to TLS and starting handshake {}", proxySession, logByteBufContent(LOG, msg));
                            proxySession.setTLS(true);
                            stateMachine.completeTLSHandshake();
                            super.channelRead(ctx, msg);
                        } else {
                            LOG.trace("Non-TLS request {}", proxySession, logByteBufContent(LOG, msg));
                            if (proxySession.isTunneling()) {
                                stateMachine.notTLSConnection();
                            }
                            logByteBufContent(LOG, msg);
                            ctx.fireChannelRead(msg);
                        }
                    } else {
                        LOG.trace("Non-TLS read {}", proxySession, logByteBufContent(LOG, msg));
                        ctx.fireChannelRead(msg);
                    }
                });
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        logByteBufContent(LOG, msg);
        ifTLS(
                () -> {
                    LOG.trace("TLS write {}", proxySession, logByteBufContent(LOG, msg));
                    super.write(ctx, msg, promise);
                },
                () -> {
                    LOG.trace("Non-TLS write {}", proxySession, logByteBufContent(LOG, msg));
                    ctx.writeAndFlush(msg, promise).addListener((ChannelFutureListener) f -> {
                        if (!f.isSuccess()) {
                            LOG.error("Failed to write non-TLS data to client", proxySession, f.cause());
                        } else {
                            LOG.debug("Non-TLS write succeeded", proxySession);
                        }
                    });
                }
        );
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.fireChannelActive();
    }

    @Override
    public void close(ChannelHandlerContext ctx, ChannelPromise promise) throws Exception {
        ctx.close(promise);
    }

    private void ifTLS(UncheckedRunnable ifTLS, UncheckedRunnable noTLS) throws Exception {
        if (proxySession.isTLS()) {
            ifTLS.run();
            return;
        } else {
            noTLS.run();
        }
    }

}
