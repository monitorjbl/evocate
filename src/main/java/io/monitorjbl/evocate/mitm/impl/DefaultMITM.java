package io.monitorjbl.evocate.mitm.impl;

import io.monitorjbl.evocate.mitm.EventSource;
import io.monitorjbl.evocate.mitm.HttpRequest;
import io.monitorjbl.evocate.mitm.HttpResponse;
import io.monitorjbl.evocate.mitm.MITM;

public class DefaultMITM implements MITM {
    @Override
    public void onConnectStart(String channelId, String host, int port) { }

    @Override
    public void onConnectComplete(String channelId) { }

    @Override
    public void onTLSStart(String channelId) { }

    @Override
    public void onTLSComplete(String channelId) { }

    @Override
    public void onRequestStart(String channelId, HttpRequest request) { }

    @Override
    public void onRequestData(String channelId, byte[] data) { }

    @Override
    public void onRequestComplete(String channelId) { }

    @Override
    public void onResponseStart(String channelId, HttpResponse response) { }

    @Override
    public void onResponseData(String channelId, byte[] data) { }

    @Override
    public void onResponseComplete(String channelId) { }

    @Override
    public void onDisconnect(String channelId, EventSource eventSource) {
    }

    @Override
    public void onError(String channelId, EventSource errorSource, Throwable cause) { }
}
