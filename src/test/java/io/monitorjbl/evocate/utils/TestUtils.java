package io.monitorjbl.evocate.utils;

import com.google.common.util.concurrent.Uninterruptibles;
import io.monitorjbl.evocate.tls.BlindTrustManager;

import javax.annotation.Nonnull;
import javax.net.ssl.*;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.*;
import java.net.http.HttpClient;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.security.SecureRandom;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class TestUtils {

    public static int getOpenPort() {
        try (var s = new ServerSocket(0)) {
            return s.getLocalPort();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static void waitForPort(int port) {
        while (true) {
            try {
                var socketChannel = SocketChannel.open();
                socketChannel.configureBlocking(true);
                try {
                    socketChannel.connect(new InetSocketAddress("localhost", port));
                    return;
                } finally {
                    socketChannel.close();
                }
            } catch (IOException e) {
                Uninterruptibles.sleepUninterruptibly(10, TimeUnit.MILLISECONDS);
            }
        }
    }

    public static URI toUri(String uri) {
        try {
            return new URI(uri);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static HttpClient httpClient(@Nonnull Optional<Integer> proxyPort) {
        System.setProperty("jdk.httpclient.redirects.retrylimit", "1");
        System.setProperty("jdk.internal.httpclient.disableHostnameVerification", "true");
        try {
            var sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{new BlindTrustManager()}, new SecureRandom());

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            var builder = HttpClient.newBuilder()
                    .version(HttpClient.Version.HTTP_1_1)
                    .sslContext(sslContext);
            proxyPort.ifPresent(integer ->
                    builder.proxy(ProxySelector.of(new InetSocketAddress("localhost", integer))));
            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
