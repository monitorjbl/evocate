package io.monitorjbl.evocate.utils;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * This class handles TLS tunneling for the WebsocketClient class
 */
public class TunnelingSocketFactory extends SocketFactory {
    private final SSLSocketFactory sslSocketFactory;
    private final String tunnelHost;
    private final int tunnelPort;
    private final String targetHost;
    private final int targetPort;

    public TunnelingSocketFactory(SSLSocketFactory sslSocketFactory, String tunnelHost, int tunnelPort, String targetHost, int targetPort) {
        this.sslSocketFactory = sslSocketFactory;
        this.tunnelHost = tunnelHost;
        this.tunnelPort = tunnelPort;
        this.targetHost = targetHost;
        this.targetPort = targetPort;
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
        return createSocket();
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
        return createSocket();
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        return createSocket();
    }

    @Override
    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        return createSocket();
    }

    @Override
    public Socket createSocket() throws IOException {
        var tunnel = setupTunnel(targetHost, targetPort);
        var socket = (SSLSocket) sslSocketFactory.createSocket(tunnel, targetHost, targetPort, true);
        socket.addHandshakeCompletedListener(
                event -> {
                    System.out.println("Handshake finished!");
                    System.out.println(
                            "\t CipherSuite:" + event.getCipherSuite());
                    System.out.println(
                            "\t SessionId " + event.getSession());
                    System.out.println(
                            "\t PeerHost " + event.getSession().getPeerHost());
                }
        );
        return socket;
    }

    private Socket setupTunnel(String host, int port) throws IOException {
        var tunnel = new Socket(tunnelHost, tunnelPort);
        doTunnelHandshake(tunnel, host, port);
        return tunnel;
    }

    /**
     * Cribbed from https://www.javaworld.com/article/2077475/java-tip-111--implement-https-tunneling-with-jsse.html
     */
    private void doTunnelHandshake(Socket tunnel, String host, int port) throws IOException {
        OutputStream out = tunnel.getOutputStream();
        String msg = "CONNECT " + host + ":" + port + " HTTP/1.0\n"
                + "User-Agent: tunneling-proxy\r\n\r\n";
        byte b[];
        try {
            /*
             * We really do want ASCII7 -- the http protocol doesn't change
             * with locale.
             */
            b = msg.getBytes("ASCII7");
        } catch (UnsupportedEncodingException ignored) {
            /*
             * If ASCII7 isn't there, something serious is wrong, but
             * Paranoia Is Good (tm)
             */
            b = msg.getBytes();
        }
        out.write(b);
        out.flush();

        /*
         * We need to store the reply so we can create a detailed
         * error message to the user.
         */
        byte reply[] = new byte[200];
        int replyLen = 0;
        int newlinesSeen = 0;
        boolean headerDone = false;     /* Done on first newline */

        InputStream in = tunnel.getInputStream();
        boolean error = false;

        while (newlinesSeen < 2) {
            int i = in.read();
            if (i < 0) {
                throw new IOException("Unexpected EOF from proxy");
            }
            if (i == '\n') {
                headerDone = true;
                ++newlinesSeen;
            } else if (i != '\r') {
                newlinesSeen = 0;
                if (!headerDone && replyLen < reply.length) {
                    reply[replyLen++] = (byte) i;
                }
            }
        }

        /*
         * Converting the byte array to a string is slightly wasteful
         * in the case where the connection was successful, but it's
         * insignificant compared to the network overhead.
         */
        String replyStr;
        try {
            replyStr = new String(reply, 0, replyLen, "ASCII7");
        } catch (UnsupportedEncodingException ignored) {
            replyStr = new String(reply, 0, replyLen);
        }

        /* We check for Connection Established because our proxy returns
         * HTTP/1.1 instead of 1.0 */
        //if (!replyStr.startsWith("HTTP/1.0 200")) {
        if (replyStr.toLowerCase().indexOf(
                "200 connection established") == -1) {
            throw new IOException("Unable to tunnel through "
                    + tunnelHost + ":" + tunnelPort
                    + ".  Proxy returns \"" + replyStr + "\"");
        }

        /* tunneling Handshake was successful! */
    }
}
