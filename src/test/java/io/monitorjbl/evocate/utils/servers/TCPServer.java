package io.monitorjbl.evocate.utils.servers;

import io.monitorjbl.evocate.utils.TestUtils;
import io.monitorjbl.evocate.utils.UncheckedInterfaces.UncheckedRunnable;
import io.monitorjbl.evocate.utils.UncheckedInterfaces.UncheckedSupplier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {
    private int port = -1;
    private ServerSocket serverSocket;

    public int getPort() {
        return port;
    }

    public TCPServer() {
        run(() -> {
            port = TestUtils.getOpenPort();
            serverSocket = new ServerSocket(port);
        });
    }

    public TCPServer(int port) {
        run(() -> {
            this.port = port;
            serverSocket = new ServerSocket(port);
        });
    }

    public TCPConnection waitForConnection() {
        return run(() -> {
            var socket = this.serverSocket.accept();
            return new TCPConnection(socket);
        });
    }

    public void close() {
        run(() -> {
            if (serverSocket != null) {
                serverSocket.close();
                port = -1;
            }
        });
    }

    public static void run(UncheckedRunnable func) {
        try {
            func.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T run(UncheckedSupplier<T> func) {
        try {
            return func.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static class TCPConnection {
        private final Socket socket;
        private final BufferedReader input;
        private final OutputStream output;

        public TCPConnection(Socket socket) {
            this.socket = socket;
            try {
                this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()), 64);
                this.output = socket.getOutputStream();
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }

        public void write(String text) {
            run(() -> output.write(text.getBytes()));
        }

        public String readLine() {
            return run(() -> input.readLine());
        }

        public void close() {
            run(() -> socket.close());
        }
    }
}
