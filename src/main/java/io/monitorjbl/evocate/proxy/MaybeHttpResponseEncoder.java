package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.session.ProxySession;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpResponseEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MaybeHttpResponseEncoder extends HttpResponseEncoder {
    private static final Logger LOG = LoggerFactory.getLogger(MaybeHttpResponseEncoder.class);
    private final ProxySession proxySession;

    public MaybeHttpResponseEncoder(ProxySession proxySession) {
        this.proxySession = proxySession;
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (!proxySession.isWebsocket() && msg instanceof HttpObject) {
            LOG.debug("Encoding HTTP response {}", proxySession, msg.getClass().getName());
            super.write(ctx, msg, promise);
        } else {
            LOG.trace("Not encoding HTTP response {}", proxySession, msg.getClass().getName());
            ctx.writeAndFlush(msg, promise);
        }
    }
}
