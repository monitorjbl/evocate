package io.monitorjbl.evocate.functional;

import io.monitorjbl.evocate.BaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.fail;

@Tag("functional-tests")
public class BrowserTest extends BaseTest {

    private WebDriver driver;

    @BeforeEach
    void openBrowser() {
        var proxy = EVOCATE_SERVER.createNewProxy();
        var options = new ChromeOptions().addArguments(
                "--test-type",
                "--ignore-certificate-errors",
                "--proxy-server=localhost:" + proxy
        );
        if(System.getProperty("evocate.test.noheadless") == null){
            options.addArguments("--headless");
        }
        driver = new ChromeDriver(options);
    }

    @AfterEach
    void closeBrowser() {
        driver.quit();
    }

    @Test
    void testBrowserProxy() throws Exception {
        driver.get("https://www.websocket.org/echo.html");

        // Wait for the page to load completely
        var wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#connect")));

        // Setup elements
        var connectBtn = driver.findElement(By.cssSelector("#connect"));
        var disconnectBtn = driver.findElement(By.cssSelector("#disconnect"));
        var sendBtn = driver.findElement(By.cssSelector("#send"));
        var messageTxt = driver.findElement(By.cssSelector("#sendMessage"));
        var consoleTxt = driver.findElement(By.cssSelector("#consoleLog"));

        // Click the Connect button to open a websocket connection
        connectBtn.click();

        // Verify that the connection worked
        waitUntil(() -> consoleTxt.getText().equals("CONNECTED"), "Websocket connection did not work");

        // Send a test message
        messageTxt.clear();
        messageTxt.sendKeys("This is a test");
        sendBtn.click();

        // Verify that a response was received
        waitUntil(() -> consoleTxt.getText().equals("CONNECTED\nSENT: This is a test\nRECEIVED: This is a test"), "Message was not sent correctly");

        // Click the disconnect button
        disconnectBtn.click();

        // Verify that the socket disconnected
        waitUntil(() -> consoleTxt.getText().equals("CONNECTED\nSENT: This is a test\nRECEIVED: This is a test\nDISCONNECTED"), "Websocket did not disconnect");
    }

    private void waitUntil(Supplier<Boolean> func, String failureMessage) {
        try {
            for (int i = 0; i < 5; i++) {
                if (func.get()) {
                    return;
                }
                Thread.sleep(1000);
            }
            fail(failureMessage);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
