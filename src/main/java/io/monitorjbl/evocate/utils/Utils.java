package io.monitorjbl.evocate.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelOutboundHandler;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.util.ReferenceCountUtil;
import org.slf4j.Logger;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URL;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Utils {
    private static final int word_width = 16;
    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();
    private static final ObjectMapper YAML_MAPPER = new ObjectMapper(new YAMLFactory());

    public static boolean isHttpConnectRequest(Object msg) {
        if (msg instanceof HttpRequest) {
            return ((HttpRequest) msg).method() == HttpMethod.CONNECT;
        }
        return false;
    }

    public static int getPort(URL url) {
        if (url.getPort() > 0) {
            return url.getPort();
        } else {
            switch (url.getProtocol()) {
                case "http":
                case "ws":
                    return 80;
                case "https":
                case "wss":
                    return 443;
                default:
                    throw new IllegalArgumentException("Unable to determine default port to use for protocol " + url.getProtocol());
            }
        }
    }

    public static String getPipelineDescription(ChannelHandlerContext ctx) {
        return getPipelineDescription(ctx.pipeline());
    }

    public static String getPipelineDescription(Channel channel) {
        return getPipelineDescription(channel.pipeline());
    }

    public static String getPipelineDescription(ChannelPipeline pipeline) {
        Function<String, String> describeHandler = n -> {
            ChannelHandler handler = pipeline.get(n);
            if (handler instanceof ChannelInboundHandler && handler instanceof ChannelOutboundHandler) {
                return "in/out";
            } else if (handler instanceof ChannelInboundHandler) {
                return "in";
            } else if (handler instanceof ChannelOutboundHandler) {
                return "out";
            } else if (handler == null) {
                return "tail";
            } else {
                return "???";
            }
        };

        return "\n" + pipeline.names().stream()
                .map(n -> String.format("%s (%s)", n, describeHandler.apply(n)))
                .map(s -> String.format("┌%s┐\n│ %s │\n└%s┘", "-".repeat(s.length() + 2), s, "-".repeat(s.length() + 2)))
                .collect(Collectors.joining(String.format("\n        ⬇\n")));
    }

    public static String logByteBufContent(Logger log, Object msg) {
        if (false && msg instanceof ByteBuf && log.isTraceEnabled()) {
            var copy = ((ByteBuf) msg).copy();
            var content = new byte[copy.readableBytes()];
            copy.readBytes(content);
            ReferenceCountUtil.release(copy);
            return bytewiseView(msg.getClass(), content);
        }
        return "";
    }

    public static String bytewiseView(Class bufClass, byte[] data) {
        // Generate left- and right- hand views
        var lh = new StringBuilder();
        var rh = new StringBuilder();
        var fused = new StringBuilder();
        fused.append("\n").append(bufClass.getName()).append(": ").append(data.length).append(" bytes").append("\n");
        for (var i = 0; i < data.length; i++) {
            if (i % word_width == 0 && i != 0) {
                fused.append("| ").append(lh).append("  |  ").append(rh).append('\n');
                lh.setLength(0);
                rh.setLength(0);
            }

            var c = (char) data[i];
            c = isPrintableChar(c) ? c : '.';
            if (c == '\n' || c == '\r' || c == ' ') {
                c = '.';
            }

            lh.append(String.format("%02x ", data[i]));
            rh.append(c);
        }

        if (lh.length() > 0) {
            fused.append("| ").append(lh).append(" ".repeat((word_width * 3) - lh.length())).append("  |  ").append(rh).append('\n');
        }

        return fused.toString();
    }

    private static boolean isPrintableChar(char c) {
        Character.UnicodeBlock block = Character.UnicodeBlock.of(c);
        return (!Character.isISOControl(c)) &&
                c != KeyEvent.CHAR_UNDEFINED &&
                block != null &&
                block != Character.UnicodeBlock.SPECIALS;
    }

    public static String dumpAsYAML(Object obj) {
        try {
            return YAML_MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static Map<String, Object> readAsJSON(InputStream is) {
        try {
            return YAML_MAPPER.readValue(is, new TypeReference<>() {});
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
