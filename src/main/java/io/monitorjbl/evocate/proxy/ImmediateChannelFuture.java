package io.monitorjbl.evocate.proxy;

import io.netty.channel.Channel;
import io.netty.channel.ChannelPromise;
import io.netty.channel.DefaultChannelPromise;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

public class ImmediateChannelFuture extends DefaultChannelPromise implements Future<Void> {
    private final Throwable cause;

    public ImmediateChannelFuture(Channel channel, Throwable cause) {
        super(channel);
        this.cause = cause;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ChannelPromise addListener(GenericFutureListener<? extends Future<? super Void>> listener) {
        return addListeners(listener);
    }

    @Override
    public Throwable cause() {
        return cause;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ChannelPromise addListeners(GenericFutureListener<? extends Future<? super Void>>... listeners) {
        GenericFutureListener[] casted = listeners;
        for (var l : casted) {
            try {
                l.operationComplete(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this;
    }
}
