package io.monitorjbl.evocate.mitm.impl;

import io.monitorjbl.evocate.mitm.ConnectionInfo;

import javax.annotation.Nonnull;

public class ConnectionInfoImpl implements ConnectionInfo {
    private final String host;
    private final int port;
    private final boolean isTls;

    public ConnectionInfoImpl(@Nonnull String host, @Nonnull int port, @Nonnull boolean isTls) {
        this.host = host;
        this.port = port;
        this.isTls = isTls;
    }

    @Override
    public String host() {
        return host;
    }

    @Override
    public int port() {
        return port;
    }

    @Override
    public boolean isTls() {
        return isTls;
    }
}
