package io.monitorjbl.evocate.utils;

import io.monitorjbl.evocate.tls.BlindTrustManager;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URI;
import java.util.function.Consumer;

public class WebsocketClient extends WebSocketClient {
    private static final Logger LOG = LoggerFactory.getLogger(WebsocketClient.class);

    private final String proxyHost;
    private final int proxyPort;
    private Consumer<String> callback;

    public WebsocketClient(URI serverURI, String proxyHost, int proxyPort) {
        super(serverURI);
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        setProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort)));
        if (serverURI.getScheme().equals("wss")) {
            setupTLS(serverURI);
        }
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        LOG.debug("opened connection");
    }

    @Override
    public void onMessage(String message) {
        LOG.debug("received: {}", message);
        if (callback != null) {
            callback.accept(message);
        }
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        LOG.debug("Connection closed by {} Code: {} Reason: {}", (remote ? "remote peer" : "us"), code, reason);
    }

    @Override
    public void onError(Exception ex) {
        LOG.error("ERROR", ex);
    }

    public void sendMessage(String msg, Consumer<String> callback) {
        this.callback = callback;
        send(msg);
    }

    private void setupTLS(URI serverURI) {
        try {
            SSLContext sslContext = null;
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{new BlindTrustManager()}, null);
            setSocketFactory(new TunnelingSocketFactory(sslContext.getSocketFactory(), proxyHost, proxyPort, serverURI.getHost(), serverURI.getPort()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
