package io.monitorjbl.evocate.functional;

import com.google.common.util.concurrent.Uninterruptibles;
import io.monitorjbl.evocate.BaseTest;
import io.monitorjbl.evocate.model.BandwidthLimit;
import io.monitorjbl.evocate.model.har.HarErrorPhase;
import io.monitorjbl.evocate.model.har.HttpMethod;
import io.monitorjbl.evocate.utils.servers.TCPServer;
import io.monitorjbl.evocate.utils.servers.TCPServer.TCPConnection;
import io.monitorjbl.evocate.utils.TestUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@Tag("functional-tests")
public class PathologicalTest extends BaseTest {
    private static final Logger LOG = LoggerFactory.getLogger(PathologicalTest.class);

    private int proxyPort;
    private TCPServer tcpServer;
    private TCPConnection proxyConnection;

    @BeforeEach
    private void setup() throws Exception {
        proxyPort = EVOCATE_SERVER.createNewProxy(new BandwidthLimit(1, 1));
        tcpServer = new TCPServer();
        proxyConnection = new TCPConnection(new Socket("localhost", proxyPort));
    }

    @AfterEach
    private void teardown() throws Exception {
        proxyConnection.close();
        tcpServer.close();
        EVOCATE_SERVER.deleteProxy(proxyPort);
    }

    @Test
    void testPainfullySlowClient() throws Exception {
        openTunnel(tcpServer.getPort());

        // Sleep for a silly amount of time
        LOG.info("Putting client to sleep");
        Uninterruptibles.sleepUninterruptibly(20, TimeUnit.SECONDS);

        // Try to send a new request
        LOG.info("Sending GET request");
        proxyConnection.write("GET / HTTP/1.1\r\nHost: localhost\r\nUser-Agent: junit/5\r\nAccept: */*\r\n\n");

        // Tell the server to wait for the request and respond with a predictable phrase
        var serverConnection = tcpServer.waitForConnection();
        assertEquals("GET / HTTP/1.1", serverConnection.readLine());
        assertEquals("Host: localhost", serverConnection.readLine());
        assertEquals("User-Agent: junit/5", serverConnection.readLine());
        assertEquals("Accept: */*", serverConnection.readLine());
        serverConnection.write("HTTP/1.1 301 Moved Permanently\r\nLocation: http://google.com\r\n\n");

        // Verify that the proxied response is correct
        assertEquals("HTTP/1.1 301 Moved Permanently", proxyConnection.readLine());
        assertEquals("Location: http://google.com", proxyConnection.readLine());
    }

    @Test
    void testPainfullySlowServer() throws Exception {
        openTunnel(tcpServer.getPort());

        // Try to send a new request
        LOG.info("Sending GET request");
        proxyConnection.write("GET / HTTP/1.1\r\nHost: localhost\r\nUser-Agent: junit/5\r\nAccept: */*\r\n\n");

        // Sleep for a silly amount of time
        LOG.info("Putting server to sleep");
        Uninterruptibles.sleepUninterruptibly(20, TimeUnit.SECONDS);

        // Tell the server to respond with a predictable phrase
        var serverConnection = tcpServer.waitForConnection();
        assertEquals("GET / HTTP/1.1", serverConnection.readLine());
        assertEquals("Host: localhost", serverConnection.readLine());
        assertEquals("User-Agent: junit/5", serverConnection.readLine());
        assertEquals("Accept: */*", serverConnection.readLine());
        serverConnection.write("HTTP/1.1 301 Moved Permanently\r\nLocation: http://google.com\r\n\n");

        assertEquals("HTTP/1.1 301 Moved Permanently", proxyConnection.readLine());
        assertEquals("Location: http://google.com", proxyConnection.readLine());
    }

    /**
     * Verifies that when a remote connection is killed, an error is logged to the HAR log
     *
     * @throws Exception
     */
    @Test
    void testErrorHarLogging() throws Exception {
        var serverPort = TestUtils.getOpenPort();
        var serverProcess = startTCPServerProcess(serverPort);
        var uploadStarted = new AtomicBoolean(false);
        var client = new Thread(() -> {
            try {
                var content = new String(ArrayUtils.toPrimitive(IntStream.range(0, 512)
                        .mapToObj(i -> (byte) (0x61 + (i % 26)))
                        .toArray(Byte[]::new)));

                proxyConnection.write("POST /disconnect HTTP/1.1\r\nContent-Length: 763746\r\n\r\n");
                uploadStarted.set(true);
                LOG.info("Upload started!");
                while (true) {
                    proxyConnection.write(content);
                    Uninterruptibles.sleepUninterruptibly(100, TimeUnit.MILLISECONDS);
                }
            } catch (Exception e) {
                LOG.info("Client terminated", e);
            }
        });

        // Open the proxy connection and start shoveling bytes
        openTunnel(serverPort);
        client.start();
        do { Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS); } while (!uploadStarted.get());

        // Terminate the connection ungracefully. This simulates a broken pipe
        // by killing the server-side socket without sending a FIN
        while(serverProcess.isAlive()) {
            serverProcess.destroyForcibly();
            Uninterruptibles.sleepUninterruptibly(500, TimeUnit.MILLISECONDS);
        }

        // Wait for the connection to be dropped on the client side
        client.join(10000);
        Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);

        // Load the HAR log
        var har = EVOCATE_SERVER.getHarLog(proxyPort);
        assertNotNull(har);
        assertNotNull(har.getLog());
        assertNotNull(har.getLog().getEntries());
        assertEquals(1, har.getLog().getEntries().size());

        // Verify that the log is correct
        var request = har.getLog().getEntries().get(0).getRequest();
        var response = har.getLog().getEntries().get(0).getResponse();
        var error = har.getLog().getEntries().get(0).getError();

        assertNotNull(request);
        assertEquals(String.format("http://localhost:%s/disconnect", serverPort), request.getUrl());
        assertEquals(HttpMethod.POST, request.getMethod());

        assertNull(response);

        assertNotNull(error);
        assertEquals(HarErrorPhase.REQUEST, error.getErrorPhase());
        assertEquals(String.format("localhost:%s", serverPort), error.getRemoteHost());
        assertEquals("Error encountered on SERVER_SIDE", error.getErrorMessage());
        assertNotNull(error.getErrorStacktrace());
    }

    private void openTunnel(int remotePort) {
        // Open a tunnel to a remote host
        LOG.info("Sending CONNECT request");
        proxyConnection.write(String.format("CONNECT localhost:%s HTTP/1.1\r\nHost: localhost:%s\r\nUser-Agent: junit/5\r\nProxy-Connection: Keep-Alive\r\n\n",
                remotePort, remotePort));

        // Verify that the tunnel is open
        assertEquals("HTTP/1.1 200 Connection established", proxyConnection.readLine());
        LOG.info("Tunnel is open");

        // Pop the empty line that signals the end of the message
        proxyConnection.readLine();
    }

    private static Process startTCPServerProcess(int port) {
        try {
            var javaBin = String.format("%s/bin/java", System.getProperty("java.home"));
            var classpath = System.getProperty("java.class.path");
            var process = new ProcessBuilder()
                    .redirectErrorStream(true)
                    .inheritIO()
                    .command(javaBin, "-cp", classpath, TCPServerProcess.class.getName(), Integer.toString(port))
                    .start();

            TestUtils.waitForPort(port);
            return process;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static class TCPServerProcess {
        private static final Logger LOG = LoggerFactory.getLogger(TCPServerProcess.class);

        public static void main(String[] args) {
            LOG.info("Starting separate TCP server in external process on port {}", args[0]);
            var server = new TCPServer(Integer.parseInt(args[0]));

            var connection = server.waitForConnection();
            LOG.info("Client connection established!");

            while (true) {
                connection.readLine();
                Uninterruptibles.sleepUninterruptibly(100, TimeUnit.MILLISECONDS);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        var port = TestUtils.getOpenPort();
        var process = startTCPServerProcess(port);

        var connection = new TCPConnection(new Socket("localhost", port));
        var client = new Thread(() -> {
            try {
                var content = new String(ArrayUtils.toPrimitive(IntStream.range(0, 10 * 1024)
                        .mapToObj(i -> (byte) 'a')
                        .toArray(Byte[]::new)));

                connection.write("POST /disconnect HTTP/1.1\r\nContent-Length: 763746\r\n\r\n");
                while (true) {
                    connection.write(content);
                }
            } catch (Exception e) {
                LOG.error("Client terminated", e);
            }
        });
        client.start();

        Uninterruptibles.sleepUninterruptibly(1000, TimeUnit.MILLISECONDS);
        process.destroyForcibly();
        client.join();
    }

}
