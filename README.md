# Evocate

![img](src/main/resources/ui/img/evocate.png)

A logging proxy, inspired by [BrowserMob Proxy](https://github.com/lightbody/browsermob-proxy). Built on Java 11 and Netty for speed and scale.

# Running

The easiest way to get started is to just run the published Docker image:

```
docker run -it -p 9090:9090 -p 39500-39510:39500-39510 monitorjbl/evocate 
```

The following environment variables are available with the default values specified below:

```
JAVA_OPTS="-Xms32m -Xmx128m"
```

The application configuration can be controlled with CLI options, as documented below:

```
Usage: evocate [-hv] [--sign-tls-certs] [--client-connection-timeout=MILLIS] [--client-keep-alive=MILLIS]
               [--client-tls-handshake-timeout=MILLIS] [--port=PORT] [--proxy-port-range=PORT_RANGE]
               [--remote-connection-timeout=MILLIS] [--remote-keep-alive=MILLIS] [--remote-tls-handshake-timeout=MILLIS]
      --client-connection-timeout=MILLIS
                         The duration in milliseconds to wait for client connections
      --client-keep-alive=MILLIS
                         The duration in milliseconds to keep client connections alive when there is no activity
      --client-tls-handshake-timeout=MILLIS
                         The duration in milliseconds to wait for client TLS handhakes
  -h, --help             Display this help message
      --port=PORT        The port to start the REST API on
      --proxy-port-range=PORT_RANGE
                         The range of ports to assign new proxies
      --remote-connection-timeout=MILLIS
                         The duration in milliseconds to wait for remote connections
      --remote-keep-alive=MILLIS
                         The duration in milliseconds to keep remote connections alive when there is no activity
      --remote-tls-handshake-timeout=MILLIS
                         The duration in milliseconds to wait for remote TLS handhakes
      --sign-tls-certs   Whether to sign all certs for MITM operations. Note that this has some significant performance
                           implications
  -v, --version          Display version info

```

# API

```
POST   /proxy                   - Create new proxy
GET    /proxy                   - Get active proxies
GET    /proxy/:port             - Get proxy by port
PUT    /proxy/:port/limits      - Sets bandwidth limits for the running proxy
PUT    /proxy/:port/har         - Start a HAR log for the running proxy
GET    /proxy/:port/har         - Get current HAR log for running proxy
DELETE /proxy/:port/har         - Stops the HAR log for running proxy
GET    /proxy/:port/sessions    - Get current active proxy sessions from running proxy
DELETE /proxy/:port             - Delete running proxy on port
```

# Developing

Building and running locally requires Java 11 and a bash terminal. Not tested on Windows at all. 

## Building

```
./gradlew build
```

## Running functional tests

Requires Chromium for browser-based tests

```
./gradlew functionalTest
```

## Running locally

Starts REST API up on port 9090

```
./gradlew clean distTar
tar xvf ./build/distributions/evocate.tar -C ./build/
./build/evocate/bin/evocate
```

## Running in docker

```
./gradlew clean docker
docker run -it -p 9090:9090 -p 39500-39510:39500-39510 evocate
```

## Releasing

Automatically bumps the current version by one patch.

```
./gradlew release
```
