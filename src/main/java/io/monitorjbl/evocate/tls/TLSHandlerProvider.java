package io.monitorjbl.evocate.tls;

import io.monitorjbl.evocate.proxy.TLSClientHandler;
import io.monitorjbl.evocate.proxy.TLSServerHandler;
import io.monitorjbl.evocate.session.ProxySession;
import io.monitorjbl.evocate.session.ProxySessionStateMachine;
import io.monitorjbl.evocate.EvocateConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.Security;

public class TLSHandlerProvider {
    private static final Logger LOG = LoggerFactory.getLogger(TLSHandlerProvider.class);
    static final String PROTOCOL = "TLSv1.3";
    static final String ALGORITHM = "SunX509";
    static final String KEYSTORE_FILE = "certs/evocate.jks";
    static final String KEYSTORE_TYPE = "JKS";
    static final String KEYSTORE_PASSWORD = "evocate";
    private static Boolean signTLSCerts = null;
    private static SSLContext serverSSLContext = null;
    private static SSLContext clientSSLContext = null;
    private static DynamicKeyManager dynamicKeyManager = null;

    public static TLSClientHandler getClientTLSHandler(EvocateConfig config, ProxySession session) {
        var sslEngine = clientSSLContext.createSSLEngine(session.getTargetHost(), session.getTargetPort());
        sslEngine.setUseClientMode(true);
        sslEngine.setNeedClientAuth(false);
        return new TLSClientHandler(config, sslEngine, session);
    }

    public static TLSServerHandler getServerTLSHandler(EvocateConfig config, ProxySessionStateMachine stateMachine, ProxySession session) {
        var sslEngine = serverSSLContext.createSSLEngine();
        sslEngine.setUseClientMode(false);
        sslEngine.setNeedClientAuth(false);
        if (signTLSCerts) {
            return new TLSServerHandler(config, new DynamicSSLEngine(dynamicKeyManager, sslEngine), stateMachine, session);
        } else {
            return new TLSServerHandler(config, sslEngine, stateMachine, session);
        }
    }

    private static void initClientSSLContext() {
        try {
            clientSSLContext = SSLContext.getInstance("TLSv1.2");
            clientSSLContext.init(null, new TrustManager[]{new BlindTrustManager()}, null);
        } catch (Exception e) {
            LOG.error("Failed to initialize the client-side SSLContext", e);
        }
    }

    private static void initServerSSLContext() {
        LOG.debug("Initiating SSL context");
        try (InputStream inputStream = TLSHandlerProvider.class.getClassLoader().getResource(KEYSTORE_FILE).openStream()) {

            var ks = KeyStore.getInstance("JKS");
            ks = KeyStore.getInstance(KEYSTORE_TYPE);
            ks.load(inputStream, KEYSTORE_PASSWORD.toCharArray());

            // Set up key manager factory to use our key store
            var kmf = KeyManagerFactory.getInstance(ALGORITHM);
            kmf.init(ks, KEYSTORE_PASSWORD.toCharArray());

            serverSSLContext = SSLContext.getInstance(PROTOCOL);
            if (signTLSCerts) {
                LOG.debug("Initializing dynamic key manager");
                DynamicKeyManager.bootstrap();
                dynamicKeyManager = new DynamicKeyManager();
                serverSSLContext.init(new KeyManager[]{dynamicKeyManager}, new TrustManager[]{new BlindTrustManager()}, null);
            } else {
                serverSSLContext.init(kmf.getKeyManagers(), new TrustManager[]{new BlindTrustManager()}, null);
            }
        } catch (Exception e) {
            LOG.error("Failed to initialize the server-side SSLContext", e);
            throw new RuntimeException(e);
        }
    }


    public static void bootstrap(EvocateConfig config) {
        TLSHandlerProvider.signTLSCerts = config.getSignTLSCerts();
        Security.setProperty("crypto.policy", "unlimited");
        initServerSSLContext();
        initClientSSLContext();
    }

}
