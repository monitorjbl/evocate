package io.monitorjbl.evocate.utils;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

public class LocalBrowser {

    public static void main(String[] args) throws Exception {
        chrome();
        while (true) {
            Thread.sleep(10000);
        }
    }

    private static void firefox() {
        var ffProfile = new FirefoxProfile();
        ffProfile.setPreference("network.proxy.type", 1);
        ffProfile.setPreference("network.proxy.http", "localhost");
        ffProfile.setPreference("network.proxy.http_port", 39500);
        ffProfile.setPreference("network.proxy.ssl", "localhost");
        ffProfile.setPreference("network.proxy.ssl_port", 39500);

        var options = new FirefoxOptions();
        options.setProfile(ffProfile);
        var driver = new FirefoxDriver(options);
    }

    private static void chrome() {
        var options = new ChromeOptions().addArguments("--proxy-server=localhost:39500");
        var driver = new ChromeDriver(options);
    }
}
