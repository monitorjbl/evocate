package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.session.ProxySession;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.websocketx.WebSocket13FrameEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MaybeWebsocketEncoder extends WebSocket13FrameEncoder {
    private static final Logger LOG = LoggerFactory.getLogger(MaybeWebsocketEncoder.class);
    private final ProxySession proxySession;

    public MaybeWebsocketEncoder(ProxySession proxySession, boolean mask) {
        super(mask);
        this.proxySession = proxySession;
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (proxySession.isWebsocket()) {
            LOG.debug("Encoding websocket request {}", proxySession, msg.getClass().getName());
            super.write(ctx, msg, promise);
        } else {
            LOG.debug("Not encoding websocket request {}", proxySession, msg.getClass().getName());
            ctx.write(msg, promise);
        }
    }
}
