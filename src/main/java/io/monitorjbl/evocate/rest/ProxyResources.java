package io.monitorjbl.evocate.rest;

import io.monitorjbl.evocate.model.ErrorMessage;
import io.monitorjbl.evocate.service.ProxyService;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;

public class ProxyResources {
    private final ProxyService proxyService;

    public ProxyResources(ProxyService proxyService) {
        this.proxyService = proxyService;
    }

    void index(RoutingContext routingContext) {
        routingContext.response().sendFile("ui/index.html");
    }

    void listProxy(RoutingContext routingContext) {
        respondJson(routingContext, 200, proxyService.listProxies());
    }

    void getProxy(RoutingContext routingContext) {
        var proxyPort = routingContext.request().getParam("port");
        var proxy = proxyService.getProxy(proxyPort);
        if (proxy.isPresent()) {
            respondJson(routingContext, 200, proxy.get());
        } else {
            respondJson(routingContext, 404, new ErrorMessage("Proxy not found"));
        }
    }

    void getSessions(RoutingContext routingContext) {
        var proxyPort = routingContext.request().getParam("port");
        var proxySessions = proxyService.getProxySessions(proxyPort);
        if (proxySessions.isPresent()) {
            respondJson(routingContext, 200, proxySessions);
        } else {
            respondJson(routingContext, 404, new ErrorMessage("Proxy not found"));
        }
    }

    void createProxy(RoutingContext routingContext) {
        try {
            respondJson(routingContext, 200, proxyService.createProxy());
        } catch (IllegalStateException e) {
            respondJson(routingContext, 409, new ErrorMessage("All proxy ports are busy!"));
        } catch (Exception e) {
            respondJson(routingContext, 500, new ErrorMessage("Internal server error!"));
        }
    }

    void deleteProxy(RoutingContext routingContext) {
        try {
            proxyService.deleteProxy(routingContext.request().getParam("port"));
            routingContext.response().setStatusCode(200).end();
        } catch (IllegalArgumentException e) {
            respondJson(routingContext, 404, new ErrorMessage("Proxy not found"));
        } catch (Exception e) {
            respondJson(routingContext, 500, new ErrorMessage("Internal server error!"));
        }
    }

    void putHar(RoutingContext routingContext) {
        try {
            respondJson(routingContext, 200, proxyService.startHar(routingContext.request().getParam("port")));
        } catch (IllegalArgumentException e) {
            respondJson(routingContext, 404, new ErrorMessage("Proxy not found"));
        } catch (Exception e) {
            respondJson(routingContext, 500, new ErrorMessage("Internal server error!"));
        }
    }

    void getHar(RoutingContext routingContext) {
        var proxyPort = routingContext.request().getParam("port");
        try {
            var har = proxyService.getProxyHar(proxyPort);
            if (har.isPresent()) {
                respondJson(routingContext, 200, har.get());
            } else {
                respondJson(routingContext, 409, new ErrorMessage("HAR logging not started"));
            }
        } catch (IllegalArgumentException e) {
            respondJson(routingContext, 404, new ErrorMessage("Proxy not found"));
        } catch (Exception e) {
            respondJson(routingContext, 500, new ErrorMessage("Internal server error!"));
        }
    }

    void deleteHar(RoutingContext routingContext) {
        try {
            respondJson(routingContext, 200, proxyService.stopHar(routingContext.request().getParam("port")));
        } catch (IllegalArgumentException e) {
            respondJson(routingContext, 404, new ErrorMessage("Proxy not found"));
        } catch (Exception e) {
            respondJson(routingContext, 500, new ErrorMessage("Internal server error!"));
        }
    }

    void putLimits(RoutingContext routingContext) {
        try {
            respondJson(routingContext, 200, proxyService.setLimits(routingContext.request().getParam("port"),
                    routingContext.request().getParam("uploadKbps"),
                    routingContext.request().getParam("downloadKbps")));
        } catch (IllegalArgumentException e) {
            respondJson(routingContext, 404, new ErrorMessage("Proxy not found"));
        } catch (Exception e) {
            respondJson(routingContext, 500, new ErrorMessage("Internal server error!"));
        }
    }

    public void close() {
        this.proxyService.close();
    }

    private static void respondJson(RoutingContext routingContext, int status, Object obj) {
        routingContext.response()
                .putHeader("content-type", "application/json")
                .setStatusCode(status)
                .end(Json.encodeToBuffer(obj));
    }
}
