package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.session.ProxySession;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequestEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MaybeHttpRequestEncoder extends HttpRequestEncoder {
    private static final Logger LOG = LoggerFactory.getLogger(MaybeHttpRequestEncoder.class);
    private final ProxySession proxySession;

    public MaybeHttpRequestEncoder(ProxySession proxySession) {
        this.proxySession = proxySession;
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (msg instanceof HttpObject) {
            LOG.debug("Encoding HTTP request {}", proxySession, msg.getClass().getName());
            super.write(ctx, msg, promise);
        } else {
            LOG.trace("Not encoding HTTP request {}", proxySession, msg.getClass().getName());
            ctx.writeAndFlush(msg, promise);
        }
    }
}
