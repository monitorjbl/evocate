package io.monitorjbl.evocate.mitm;

public enum HttpMethod {
    GET, POST, PUT, DELETE, PATCH, OPTIONS, CONNECT, TRACE
}
