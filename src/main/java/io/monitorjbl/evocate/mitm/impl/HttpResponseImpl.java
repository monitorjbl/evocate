package io.monitorjbl.evocate.mitm.impl;

import io.monitorjbl.evocate.mitm.HttpHeaders;
import io.monitorjbl.evocate.mitm.HttpProtocol;
import io.monitorjbl.evocate.mitm.HttpResponse;

import javax.annotation.Nonnull;

import static java.util.stream.Collectors.toList;

public class HttpResponseImpl implements HttpResponse {
    private final HttpProtocol protocol;
    private final int statusCode;
    private final String statusText;
    private final HttpHeaders headers;

    public HttpResponseImpl(@Nonnull HttpProtocol protocol, @Nonnull int statusCode, @Nonnull String statusText, @Nonnull HttpHeaders headers) {
        this.protocol = protocol;
        this.statusCode = statusCode;
        this.statusText = statusText;
        this.headers = headers;
    }

    @Override
    public HttpProtocol protocol() {
        return protocol;
    }

    @Override
    public int statusCode() {
        return statusCode;
    }

    @Override
    public String statusText() {
        return statusText;
    }

    @Override
    public HttpHeaders headers() {
        return headers;
    }

    @Override
    public String toString() {
        return protocol + " " + statusCode + " " + statusCode + "\n" + headers;
    }

    public static HttpResponse from(io.netty.handler.codec.http.HttpResponse res) {
        var protocol = HttpProtocol.from(res.protocolVersion());
        var headers = new HttpHeadersImpl(res.headers().names().stream()
                .map(n -> new HttpHeaderImpl(n, res.headers().getAll(n)))
                .collect(toList()));
        return new HttpResponseImpl(protocol, res.status().code(), res.status().reasonPhrase(), headers);
    }
}
