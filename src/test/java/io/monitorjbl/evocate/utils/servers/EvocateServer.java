package io.monitorjbl.evocate.utils.servers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.monitorjbl.evocate.EvocateApp;
import io.monitorjbl.evocate.EvocateConfig;
import io.monitorjbl.evocate.model.BandwidthLimit;
import io.monitorjbl.evocate.model.har.Har;
import io.monitorjbl.evocate.rest.EvocateRestServer;
import io.monitorjbl.evocate.rest.RestServer;
import io.monitorjbl.evocate.tls.TLSHandlerProvider;
import io.monitorjbl.evocate.utils.TestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static io.monitorjbl.evocate.utils.TestUtils.toUri;
import static java.util.stream.Collectors.toList;

public class EvocateServer {
    private static final Logger LOG = LoggerFactory.getLogger(EvocateServer.class);
    private static final String proxy_port_range = "45673-45675";
    private static final HttpClient client = TestUtils.httpClient(Optional.empty());
    private static final ObjectMapper object_mapper = new ObjectMapper();

    private final RestServer restServer;
    private final int restPort;

    public EvocateServer(RestServer restServer, int restPort) {
        this.restServer = restServer;
        this.restPort = restPort;
    }

    public void stopServer() {
        LOG.info("Stopping test server on {}", restPort);
        try {
            restServer.close();
            LOG.info("Test server has shut down");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public int createNewProxy() {
        return createNewProxy(Optional.empty());
    }

    public int createNewProxy(Optional<BandwidthLimit> bandwidthLimit) {
        var request = HttpRequest.newBuilder()
                .uri(toUri(String.format("http://localhost:%s/proxy", restPort)))
                .POST(HttpRequest.BodyPublishers.ofByteArray(new byte[0]))
                .build();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());

            var json = object_mapper.readValue(response.body(), Map.class);
            var port = (int) json.get("port");
            LOG.info("Created new test proxy on {}", port);

            request = HttpRequest.newBuilder()
                    .uri(toUri(String.format("http://localhost:%s/proxy/%s/har", restPort, port)))
                    .PUT(HttpRequest.BodyPublishers.ofByteArray(new byte[0]))
                    .build();
            client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            LOG.info("Started HAR log on proxy {}", port);

            if (bandwidthLimit.isPresent()) {
                client.send(HttpRequest.newBuilder()
                        .uri(toUri(String.format("http://localhost:%s/proxy/%s/limits?uploadKbps=%s&downloadKbps=%s",
                                restPort,
                                port,
                                bandwidthLimit.get().getUploadKbps(),
                                bandwidthLimit.get().getDownloadKbps())))
                        .PUT(HttpRequest.BodyPublishers.ofByteArray(new byte[0]))
                        .build(), HttpResponse.BodyHandlers.ofByteArray());
            }

            return port;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public int createNewProxy(BandwidthLimit bandwidthLimit) {
        var proxyPort = createNewProxy();
        var request = HttpRequest.newBuilder()
                .uri(toUri(String.format("http://localhost:%s/proxy/%s/limits?uploadKbps=%s&downloadKbps=%s",
                        restPort,
                        proxyPort,
                        bandwidthLimit.getUploadKbps(),
                        bandwidthLimit.getDownloadKbps())))
                .PUT(HttpRequest.BodyPublishers.ofByteArray(new byte[0]))
                .build();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            var json = object_mapper.readValue(response.body(), Map.class);
            LOG.info("Set bandwidth limits on proxy {}", json.get("port"));
            return (int) json.get("port");
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public Har getHarLog(int proxyPort) {
        var request = HttpRequest.newBuilder()
                .uri(toUri(String.format("http://localhost:%s/proxy/%s/har", restPort, proxyPort)))
                .GET()
                .build();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            return object_mapper.readValue(response.body(), Har.class);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteProxy(int port) {
        var request = HttpRequest.newBuilder()
                .uri(toUri(String.format("http://localhost:%s/proxy/%s", restPort, port)))
                .DELETE()
                .build();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            LOG.info("Deleted test proxy on {}", port);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void clearProxies() {
        var request = HttpRequest.newBuilder()
                .uri(toUri(String.format("http://localhost:%s/proxy", restPort)))
                .GET()
                .build();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());
            List<Map> json = object_mapper.readValue(response.body(), new TypeReference<List<Map>>() {});
            json.stream().forEach(m -> deleteProxy((Integer) m.get("port")));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static EvocateServer startServer() {
        var port = TestUtils.getOpenPort();
        var config = EvocateConfig.builder()
                .withApiPort(port)
                .withProxyPortRange(proxy_port_range)
                .withSignTLSCerts(false)
                .build();
        TLSHandlerProvider.bootstrap(config);
        LOG.info("Starting test server on {}", port);
        var server = new EvocateRestServer(config);
        server.run();

        TestUtils.waitForPort(port);

        return new EvocateServer(server, port);
    }

    public static EvocateServer startServerExternally() {
        try {
            var port = TestUtils.getOpenPort();
            var config = EvocateConfig.builder()
                    .withApiPort(port)
                    .withProxyPortRange(proxy_port_range)
                    .withSignTLSCerts(false)
                    .build();
            var javaBin = String.format("%s/bin/java", System.getProperty("java.home"));
            var classpath = System.getProperty("java.class.path");
            var command = Stream.concat(
                    Stream.of(javaBin, "-cp", classpath, EvocateApp.class.getName()),
                    config.toArgs().stream()).collect(toList());

            var process = new ProcessBuilder()
                    .redirectErrorStream(true)
                    .command(command)
                    .start();
            TestUtils.waitForPort(port);

            return new EvocateServer(new ExternalRestServer(process), port);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static class ProcessLogger implements Runnable {
        private static final String ANSI_GREEN = "\u001B[32m";
        private static final String ANSI_RESET = "\u001B[0m";
        private final ExternalRestServer restServer;

        public ProcessLogger(ExternalRestServer restServer) {
            this.restServer = restServer;
        }

        @Override
        public void run() {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(restServer.process.getInputStream()))) {
                while (restServer.isRunning()) {
                    System.out.println(ANSI_GREEN + reader.readLine() + ANSI_RESET);

                }
            } catch (IOException e) {
                LOG.info("Process stdout is closed");
            }
        }
    }

    private static class ExternalRestServer implements RestServer {
        private final Process process;
        private final Thread logger;

        public ExternalRestServer(Process process) {
            this.process = process;
            this.logger = new Thread(new ProcessLogger(this));
            this.logger.start();
        }

        @Override
        public void run() { }

        @Override
        public boolean isRunning() {
            return process.isAlive();
        }

        @Override
        public void close() {
            try {
                process.destroy();
                process.waitFor();
                this.logger.join();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
