package io.monitorjbl.evocate.session;

import io.monitorjbl.evocate.EvocateConfig;
import io.monitorjbl.evocate.mitm.EventSource;
import io.monitorjbl.evocate.mitm.impl.HttpRequestImpl;
import io.monitorjbl.evocate.mitm.impl.HttpResponseImpl;
import io.monitorjbl.evocate.model.ProxySessionState;
import io.monitorjbl.evocate.proxy.RemoteChannel;
import io.monitorjbl.evocate.store.ProxySessionStore;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.EventLoopGroup;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.DefaultHttpRequest;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.util.CharsetUtil;
import io.netty.util.ReferenceCountUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.nio.channels.ClosedChannelException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static io.monitorjbl.evocate.model.ProxySessionState.CONNECTED;
import static io.monitorjbl.evocate.model.ProxySessionState.SESSION_INITIATING;
import static io.monitorjbl.evocate.utils.Utils.getPort;
import static io.monitorjbl.evocate.utils.Utils.isHttpConnectRequest;

public class ProxySessionStateMachine {
    private static final Logger LOG = LoggerFactory.getLogger(ProxySessionStateMachine.class);
    private static final String PROXY_SUCCESS_RESPONSE = "%s 200 Connection established\r\n\n";
    private static final String PROXY_FAILURE_RESPONSE = "%s 500 Connection failed\r\n\n";

    public enum Direction {OUT, IN}

    private final EvocateConfig config;
    private final ProxySessionStore proxySessionStore;
    private final ProxySession proxySession;
    private final EventLoopGroup serverFacingGroup;
    private final Queue<Object> backupRequestQueue = new ConcurrentLinkedQueue<>();
    private final Queue<Object> backupResponseQueue = new ConcurrentLinkedQueue<>();
    private final Lock channelLock = new ReentrantLock();

    public ProxySessionStateMachine(EvocateConfig config, ProxySessionStore proxySessionStore, ProxySession proxySession, EventLoopGroup serverFacingGroup) {
        this.config = config;
        this.proxySessionStore = proxySessionStore;
        this.proxySession = proxySession;
        this.serverFacingGroup = serverFacingGroup;
    }

    private ChannelFuture connect(ChannelHandlerContext ctx, HttpRequest httpRequest) throws Exception {
        // If this message is an HTTP request, we are one of two states: either this is a
        // CONNECT request that indicates the client is requesting an HTTP proxy tunnel
        // or the request is a normal HTTP request that should be forwarded directly to
        // a remote host.
        var promise = ctx.newPromise();
        if (isHttpConnectRequest(httpRequest)) {
            LOG.debug("Received a CONNECT request", proxySession);
            proxySession.setTunneling(true);

            // Initiate proxy-to-server connection
            var split = httpRequest.uri().split(":");
            var host = split[0];
            var port = Integer.parseInt(split[1]);

            proxySession.getMitm().onConnectStart(mitmChannelId(), host, port);
            addServerConnection(host, port).openConnection().addListener(future -> {
                if (!future.isSuccess()) {
                    LOG.warn("Failed to connect to {}, aborting session", proxySession, proxySession.getServerChannel().getHost());
                    ctx.writeAndFlush(Unpooled.copiedBuffer(
                            String.format(PROXY_FAILURE_RESPONSE, httpRequest.protocolVersion().toString()),
                            CharsetUtil.US_ASCII
                    ));
                    ctx.channel().close();
                    return;
                }

                // Respond to client request so it begins the real connection
                proxySession.getMitm().onConnectComplete(mitmChannelId());
                changeSessionState(ProxySessionState.AWAITING_CONNECTION);
                writeToClient(Unpooled.copiedBuffer(
                        String.format(PROXY_SUCCESS_RESPONSE, httpRequest.protocolVersion().toString()),
                        CharsetUtil.US_ASCII
                )).addListener(f -> {
                    if (f.isSuccess()) {
                        LOG.debug("Proxy session established", proxySession);
                        proxySession.getMitm().onTLSStart(mitmChannelId());
                        promise.setSuccess();
                    } else {
                        LOG.error("Failed to establish proxy session", proxySession, f.cause());
                        ctx.channel().close();
                    }
                });
            });
        } else {
            LOG.debug("Received a bare HTTP request", proxySession);
            var url = new URL(httpRequest.uri());
            var host = url.getHost();
            var port = url.getPort();

            // The port may not be provided, so try to infer it
            if (port < 1) {
                if (url.getProtocol().equals("http")) {
                    port = 80;
                } else if (url.getProtocol().equals("https")) {
                    port = 443;
                } else {
                    LOG.error("Could not infer port from {}!", url);
                    disconnect("Failed to infer port", EventSource.CLIENT_SIDE);
                    return ctx.newFailedFuture(new IllegalArgumentException("Could not infer port"));
                }
            }

            proxySession.getMitm().onConnectStart(mitmChannelId(), host, port);
            changeSessionState(ProxySessionState.AWAITING_CONNECTION);
            addServerConnection(host, port).openConnection().addListener(future -> {
                if (!future.isSuccess()) {
                    ctx.channel().close();
                    return;
                }

                // Mangle the request so the server-side client can deal with it
                proxySession.getMitm().onConnectComplete(mitmChannelId());
                var hostname = String.format("%s:%s", url.getHost(), getPort(url));
                var mangledHeaders = new DefaultHttpHeaders();
                httpRequest.headers().names().forEach(n -> {
                    if (n.equals("Host")) {
                        mangledHeaders.set(n, hostname);
                    } else {
                        mangledHeaders.set(n, httpRequest.headers().getAll(n));
                    }
                });
                var mangledRequest = new DefaultHttpRequest(
                        httpRequest.protocolVersion(),
                        httpRequest.method(),
                        url.getPath(),
                        mangledHeaders
                );

                sessionStateMachine(ctx, Direction.OUT, mangledRequest, () -> changeSessionState(ProxySessionState.CONNECTED));
                promise.setSuccess();
            });
        }

        return promise;
    }

    private RemoteChannel addServerConnection(String targetHost, int targetPort) {
        if (proxySession.isTunneling()) {
            LOG.info("Starting new tunneling session to {}:{}", proxySession, targetHost, targetPort);
        } else {
            LOG.info("Starting new HTTP session to {}:{}", proxySession, targetHost, targetPort);
        }
        proxySession.setServerChannel(new RemoteChannel(config, serverFacingGroup, this, proxySession, targetHost, targetPort, proxySession.getMitm()));
        proxySession.setTargetHost(targetHost);
        proxySession.setTargetPort(targetPort);
        return proxySession.getServerChannel();
    }

    // Signifies that this connection is tunneling and using TLS
    public void completeTLSHandshake() {
        changeSessionState(ProxySessionState.HANDSHAKE_COMPLETE);
    }

    // Signifies that this connection is tunneling but not using TLS
    public void notTLSConnection() {
        changeSessionState(CONNECTED);
    }

    public void channelError(EventSource eventSource, Throwable cause) {
        proxySession.getMitm().onError(mitmChannelId(), eventSource, cause);
        disconnect("Channel error on " + eventSource, eventSource);
    }

    public ChannelFuture sessionStateMachine(ChannelHandlerContext ctx, Direction direction, Object msg) throws Exception {
        return sessionStateMachine(ctx, direction, msg, null);
    }

    private ChannelFuture sessionStateMachine(ChannelHandlerContext ctx, Direction direction, Object msg, Runnable beforeOp) throws Exception {
        // This warrants some explanation. Connecting is a mandatorily single-thread process. We
        // need to prevent any other messages from coming through before the connection is fully
        // established. To do this, we will lock the chennel and completely write the connection
        // request before unlocking.
        // We also want to be able to support atomic operations alongside a state machine
        // procession. In these instances, the channel will be locked before the operation begins
        // and released after it completes.
        var shouldLock = proxySession.getCurrentState().getOrder() <= CONNECTED.getOrder() || beforeOp != null;

        if (shouldLock) {
            channelLock.lock();
        }
        if (beforeOp != null) {
            beforeOp.run();
        }

        try {
            switch (proxySession.getCurrentState()) {
                case START:
                    // This state indicates that the session has just started and no actions have been taken yet. The
                    // request we are seeing is the first one and will either be an HTTP CONNECT requestion, which
                    // indicates that we must set up a tunneling session or a bare HTTP request with a Host header set.
                    if (msg instanceof HttpRequest) {
                        changeSessionState(SESSION_INITIATING);
                        return connect(ctx, (HttpRequest) msg);
                    } else {
                        LOG.error("Did not receive an HTTP request to start, got {}!", msg.getClass().getName());
                        return ctx.newFailedFuture(new IllegalArgumentException("Did not receive starting HTTP request"));
                    }
                case SESSION_INITIATING:
                    // If this is a tunneling request, we need to return and allow the client to send the
                    // data it wants proxied. If we are not tunneling, then advance ahead to CONNECTED
                    // state so we can start shoveling bytes.
                    if (!proxySession.isTunneling()) {
                        changeSessionState(ProxySessionState.CONNECTED);
                        return sessionStateMachine(ctx, direction, msg);
                    } else if (msg instanceof LastHttpContent) {
                        LOG.debug("Ignoring empty HTTP message, session is still initiating", proxySession);
                    } else {
                        changeSessionState(ProxySessionState.AWAITING_CONNECTION);
                    }
                    return ctx.newSucceededFuture();
                case AWAITING_CONNECTION:
                    if (direction == Direction.OUT) {
                        LOG.info("Backing up outbound messge until state enters SENDING");
                        backupOutboundMessage(msg);
                        return ctx.newSucceededFuture();
                    } else {
                        LOG.warn("It should not be possible for this state!", proxySession);
                        return ctx.newFailedFuture(new IllegalStateException("It should not be possible to be in this state"));
                    }
                case HANDSHAKE_COMPLETE:
                    LOG.debug("Handshake is complete");
                    proxySession.getMitm().onTLSComplete(mitmChannelId());
                    changeSessionState(ProxySessionState.CONNECTED);
                    return sessionStateMachine(ctx, direction, msg);
                case CONNECTED:
                    LOG.debug("Connected", proxySession);
                    if (direction == Direction.OUT) {
                        dispatchMitm(proxySession, direction, msg);
                        changeSessionState(ProxySessionState.SENDING);
                        return writeToServer(msg).addListener(f -> {
                            flushBackupQueue(backupRequestQueue, Direction.OUT, ctx);
                        });
                    } else if (direction == Direction.IN) {
                        LOG.debug("Backing up inbound message {} until state enters RECEIVING", proxySession, msg.getClass().getName());
                        backupInboundMessage(msg);
                        return ctx.newSucceededFuture();
                    }
                case SENDING:
                    LOG.debug("Sending {}", proxySession, msg);
                    if (direction == Direction.OUT) {
                        dispatchMitm(proxySession, direction, msg);
                        return writeToServer(msg).addListener(f -> {
                            if (msg instanceof LastHttpContent) {
                                LOG.debug("Detected end of initial request");
                                changeSessionState(ProxySessionState.RECEIVING);
                            }
                        });
                    } else if (direction == Direction.IN) {
                        LOG.debug("Backing up inbound message {} until state enters RECEIVING", proxySession, msg.getClass().getName());
                        backupInboundMessage(msg);
                        return ctx.newSucceededFuture();
                    }
                case RECEIVING:
                    if (proxySession.isWebsocket()) {
                        changeSessionState(ProxySessionState.TUNNELING);
                        return flushBackupQueue(backupResponseQueue, direction, ctx);
                    } else {
                        if (direction == Direction.IN) {
                            dispatchMitm(proxySession, direction, msg);
                            return writeToClient(msg);
                        } else {
                            LOG.warn("Dropping outbound request: {}", proxySession, msg);
                            return ctx.newSucceededFuture();
                        }
                    }
                case TUNNELING:
                    LOG.debug("Tunneling {}", proxySession, msg);
                    dispatchMitm(proxySession, direction, msg);
                    if (direction == Direction.IN) {
                        return writeToClient(msg);
                    } else if (direction == Direction.OUT) {
                        return writeToServer(msg);
                    } else {
                        LOG.error("Illegal direction for tunneling: {}", direction);
                        return ctx.newFailedFuture(new IllegalArgumentException("Illegal direction"));
                    }
                case DISCONNECTING:
                    LOG.debug("Session is shut down", proxySession);
                    return ctx.newSucceededFuture();
                default:
                    LOG.warn("Not sure how to handle state {}", proxySession, proxySession.getCurrentState());
                    return ctx.newFailedFuture(new IllegalStateException("Can't handle state " + proxySession.getCurrentState()));
            }
        } finally {
            if (shouldLock) {
                channelLock.unlock();
            }
        }
    }

    public void disconnect(String reason, EventSource eventSource) {
        try {
            LOG.debug("Disconnecting session: {}", proxySession, reason);
            proxySession.getMitm().onDisconnect(mitmChannelId(), eventSource);
            proxySessionStore.remove(proxySession.getId());
            purgeBackupQueue(backupRequestQueue);
            purgeBackupQueue(backupResponseQueue);
            proxySession.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private synchronized void changeSessionState(ProxySessionState state) {
        LOG.debug("Changing session from {} to {}", this, proxySession.getCurrentState(), state);
        proxySession.setCurrentState(state);
    }

    public ChannelFuture writeToServer(Object msg) {
        LOG.debug("Writing {} to server", this, msg.getClass().getName());
        return proxySession.getServerChannel().writeAndFlush(msg).addListener(future -> {
            if (future.isSuccess()) {
                LOG.debug("Server write of {} succeeded", this, msg.getClass().getName());
            } else {
                if (future.cause() instanceof ClosedChannelException) {
                    LOG.info("Server write of {} failed due to disconnect", this, msg.getClass().getName());
                } else {
                    LOG.warn("Server write of {} failed", this, msg.getClass().getName(), future.cause());
                    channelError(EventSource.SERVER_SIDE, future.cause());
                }
            }
        });
    }

    public ChannelFuture writeToClient(Object msg) {
        LOG.debug("Writing {} to client", this, msg.getClass().getName());
        return proxySession.getClientSideChannel().writeAndFlush(msg).addListener(future -> {
            if (future.isSuccess()) {
                LOG.debug("Client write of {} succeeded", this, msg.getClass().getName());
            } else {
                if (future.cause() instanceof ClosedChannelException) {
                    LOG.info("Client write of {} failed due to diconnect", this, msg.getClass().getName());
                } else {
                    LOG.warn("Client write of {} failed", this, msg.getClass().getName(), future.cause());
                    channelError(EventSource.SERVER_SIDE, future.cause());
                }
            }
        });
    }

    private void dispatchMitm(ProxySession proxySession, Direction direction, Object msg) {
        switch (direction) {
            case OUT:
                dispatchMitmRequest(msg);
                return;
            case IN:
                dispatchMitmResponse(msg);
                return;
            default:
                throw new IllegalStateException("Unknown direction: " + direction);
        }
    }

    private void dispatchMitmRequest(Object msg) {
        try {
            var mitm = proxySession.getMitm();
            if (msg instanceof HttpRequest) {
                var request = HttpRequestImpl.from(proxySession, (HttpRequest) msg);
                LOG.info("HTTPRequest: {} {}{}", proxySession, request.method(), request.connectionInfo().host(), request.target());
                mitm.onRequestStart(mitmChannelId(), request);
                return;
            }
            if (msg instanceof HttpContent) {
                var copy = ((HttpContent) msg).copy();
                var content = new byte[copy.content().readableBytes()];
                copy.content().readBytes(content);
                mitm.onRequestData(mitmChannelId(), content);
                ReferenceCountUtil.release(content);
            }
            if (msg instanceof LastHttpContent) {
                mitm.onRequestComplete(mitmChannelId());
            }
        } catch (Exception e) {
            LOG.error("Failed to call MITM", e);
        }
    }

    private void dispatchMitmResponse(Object msg) {
        try {
            var mitm = proxySession.getMitm();
            if (msg instanceof HttpResponse) {
                var response = HttpResponseImpl.from((HttpResponse) msg);
                LOG.info("HTTPResponse: {} {}", proxySession, response.statusCode(), response.statusText());
                mitm.onResponseStart(mitmChannelId(), response);
                return;
            }
            if (msg instanceof HttpContent) {
                var copy = ((HttpContent) msg).copy();
                var content = new byte[copy.content().readableBytes()];
                copy.content().readBytes(content);
                mitm.onResponseData(mitmChannelId(), content);
                ReferenceCountUtil.release(copy);
            }
            if (msg instanceof LastHttpContent) {
                mitm.onResponseComplete(mitmChannelId());
            }
        } catch (Exception e) {
            LOG.error("Failed to call MITM", e);
        }
    }

    private void backupInboundMessage(Object msg) {
        backupResponseQueue.add(msg);
    }

    private void backupOutboundMessage(Object msg) {
        backupRequestQueue.add(msg);
    }

    private ChannelPromise flushBackupQueue(Queue<Object> backupQueue, Direction direction, ChannelHandlerContext ctx) throws Exception {
        var size = new AtomicInteger(backupQueue.size());
        var promise = ctx.newPromise();
        LOG.debug("Flushing queue with {} messages", proxySession, size.get());
        while (backupQueue.size() > 0) {
            var m = backupQueue.poll();
            sessionStateMachine(ctx, direction, m).addListener(f -> {
                LOG.debug("Message {} flushed", proxySession, m);
                if (size.decrementAndGet() == 0) {
                    promise.setSuccess();
                }
            });
        }
        return promise;
    }

    private void purgeBackupQueue(Queue<Object> backupQueue) {
        LOG.debug("Purging backup queue ({} messages)", proxySession, backupQueue.size());
        for (var m : backupQueue) {
            ReferenceCountUtil.release(m);
        }
        backupQueue.clear();
    }

    private String mitmChannelId() {
        return proxySession.getId().asShortText();
    }
}
