package io.monitorjbl.evocate.mitm.impl;

import com.google.common.collect.ImmutableList;
import io.monitorjbl.evocate.mitm.HttpHeader;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

public class HttpHeaderImpl implements HttpHeader {

    private final String name;
    private final List<String> values;

    public HttpHeaderImpl(@Nonnull String name, @Nonnull List<String> values) {
        this.name = name;
        this.values = values;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String get() {
        return values.get(0);
    }

    @Override
    public List<String> values() {
        return ImmutableList.copyOf(values);
    }

    @Override
    public String toString() {
        return name + ":" + values.stream().collect(Collectors.joining(","));
    }
}
