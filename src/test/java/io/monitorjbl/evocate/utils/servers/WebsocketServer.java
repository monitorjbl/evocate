package io.monitorjbl.evocate.utils.servers;

import io.monitorjbl.evocate.utils.WebsocketClient;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.net.JksOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;

import static io.monitorjbl.evocate.utils.TestUtils.getOpenPort;

public class WebsocketServer extends AbstractVerticle {
    private static final Logger LOG = LoggerFactory.getLogger(WebsocketServer.class);
    private static int http_port = -1;
    private static int https_port = -1;
    private static WebsocketServer server;

    private WebsocketServer() {
        this.vertx = Vertx.vertx();
        HttpServer http = vertx.createHttpServer();
        HttpServer https = vertx.createHttpServer(new HttpServerOptions()
                .setSsl(true)
                .setKeyStoreOptions(new JksOptions()
                        .setPath(WebsocketServer.class.getClassLoader().getResource("certs/evocate.jks").getFile())
                        .setPassword("evocate")));

        http.webSocketHandler(handleMessages(false)).listen(http_port);
        https.webSocketHandler(handleMessages(true)).listen(https_port);
    }

    private Handler<ServerWebSocket> handleMessages(boolean tls) {
        return ctx -> {
            ctx.textMessageHandler((msg) -> {
                LOG.info("Received message (tls:{}): {}", tls, msg);
                ctx.writeTextMessage(msg);
            });
        };
    }

    public static void startServer() {
        if (server == null) {
            http_port = getOpenPort();
            https_port = getOpenPort();
            LOG.info("Starting test websocket server on http:{}/https:{}", http_port, https_port);
            server = new WebsocketServer();
        }
    }

    public static void stopServer() {
        if (server != null) {
            LOG.info("Stopping test server on http:{}/https:{}", http_port, https_port);
            try {
                server.vertx.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                http_port = -1;
                https_port = -1;
                server = null;
            }
        }
    }

    public static WebsocketClient getWSClient(int proxy) {
        try {
            return new WebsocketClient(new URI("ws://localhost:" + http_port), "localhost", proxy);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static WebsocketClient getWSSClient(int proxy) {
        try {
            return new WebsocketClient(new URI("wss://localhost:" + https_port), "localhost", proxy);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
