# TODO

* Implement proxy timeouts for reaping
* Implement disk-backed Map for each HAR to avoid memory overflows
* Make many more things configurable
    * Content truncation limit
    * Bind address
    * TLS certs 
    * Disable header/cookie/content capture
