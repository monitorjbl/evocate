package io.monitorjbl.evocate.proxy;

import io.monitorjbl.evocate.session.ProxySession;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequestDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MaybeHttpRequestDecoder extends HttpRequestDecoder {
    private static final Logger LOG = LoggerFactory.getLogger(MaybeHttpRequestDecoder.class);
    private final ProxySession proxySession;

    public MaybeHttpRequestDecoder(ProxySession proxySession) {
        this.proxySession = proxySession;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (!proxySession.isWebsocket()) {
            LOG.debug("Decoding HTTP request {}", proxySession, msg.getClass().getName());
            super.channelRead(ctx, msg);
        } else {
            LOG.trace("Not decoding HTTP request {}", proxySession, msg.getClass().getName());
            ctx.fireChannelRead(msg);
        }
    }
}
