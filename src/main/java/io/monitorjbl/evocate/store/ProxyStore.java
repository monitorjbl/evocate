package io.monitorjbl.evocate.store;

import io.monitorjbl.evocate.model.Proxy;

import java.util.List;
import java.util.stream.Collectors;

public class ProxyStore extends AbstractStore<String, Proxy> {
    @Override
    public List<Proxy> listValues() {
        return super.listValues().stream()
                .sorted(Proxy::compareTo)
                .collect(Collectors.toList());
    }
}
