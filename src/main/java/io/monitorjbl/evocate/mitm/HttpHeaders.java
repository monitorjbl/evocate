package io.monitorjbl.evocate.mitm;

import java.util.Set;

public interface HttpHeaders {
    Set<String> names();

    String get(String name);
}
