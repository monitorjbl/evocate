package io.monitorjbl.evocate.stress;

import com.google.common.util.concurrent.Uninterruptibles;
import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.monitorjbl.evocate.utils.servers.EvocateServer;
import io.monitorjbl.evocate.utils.servers.HttpServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Tag("stress-tests")
public class StressTest {
    private static final Logger LOG = LoggerFactory.getLogger(StressTest.class);
    private int proxyPort = 39500;
    private EvocateServer evocateServer;

    @BeforeEach
    private void setup() throws Exception {
        evocateServer = EvocateServer.startServerExternally();
        proxyPort = evocateServer.createNewProxy();
    }

    @AfterEach
    private void teardown() throws Exception {
        evocateServer.stopServer();
    }

    @BeforeAll
    static void startHttp() {
        HttpServer.startServer();
    }

    @AfterAll
    static void stopHttp() {
        HttpServer.stopServer();
    }

    /**
     * Runs a basic stress test against an externally-started Evocate server using
     * fast, lightweight HTTP requests.
     */
    @ParameterizedTest
    @ValueSource(ints = {10, 20})
    public void fastClients(int requestsPerSecond) {
        var clients = Math.max(1, requestsPerSecond / 50);
        var clientPool = Executors.newFixedThreadPool(clients);
        var requestBucket = Bucket4j.builder()
                .addLimit(Bandwidth.simple(requestsPerSecond, Duration.ofSeconds(1)))
                .build();
        var running = new AtomicBoolean(true);
        var successes = new AtomicLong(0);
        var failures = new AtomicLong(0);
        var clientFutures = IntStream.range(0, clients).mapToObj(i -> clientPool.submit(() -> {
            while (running.get()) {
                clientBehavior(requestBucket, successes, failures, () -> makeSmallRequest(proxyPort));
            }
        })).collect(toList());

        Uninterruptibles.sleepUninterruptibly(15, TimeUnit.SECONDS);
        running.set(false);
        clientFutures.forEach(f -> {
            try {
                f.get();
            } catch (InterruptedException | ExecutionException e) {
                LOG.error("Client future failure", e);
            }
        });

        System.out.printf("Report:\n  Success: %s\n  Failure: %s%n\n", successes.get(), failures.get());

        assertEquals(0, failures.get(), "there were client failures");
    }

    private static void clientBehavior(Bucket requestBucket, AtomicLong successes, AtomicLong failures, Runnable clientFunc) {
        try {
            // Fetch a request token and sleep as required to throttle ourselves
            requestBucket.asScheduler().consume(1);
            clientFunc.run();
            successes.incrementAndGet();
        } catch (Exception e) {
            LOG.error("Client request failed", e);
            failures.incrementAndGet();
        }
    }

    private static void makeSmallRequest(int proxy) {
        HttpServer.httpRequest(proxy, "/small/endpoint", r -> r.GET().build());
    }
}
